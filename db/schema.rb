# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181002022618) do

  create_table "asentamientos", force: :cascade do |t|
    t.string   "nombre",               limit: 4000
    t.integer  "tipo_asentamiento_id", limit: 4
    t.integer  "codigo_postal",        limit: 4
    t.integer  "ciudad_id",            limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "bancos", force: :cascade do |t|
    t.string   "nombre",     limit: 4000
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "beneficiarios", force: :cascade do |t|
    t.integer  "socio_id",   limit: 4
    t.string   "nombre",     limit: 4000
    t.float    "porcentaje"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "beneficiarios", ["socio_id"], name: "index_beneficiarios_on_socio_id"

  create_table "categories", force: :cascade do |t|
    t.string   "categoria",     limit: 4000
    t.integer  "val1",          limit: 4
    t.integer  "val2",          limit: 4
    t.string   "vals1",         limit: 4000
    t.string   "vals2",         limit: 4000
    t.integer  "parent_id",     limit: 4
    t.string   "tree_path",     limit: 4000
    t.boolean  "activo"
    t.boolean  "has_child"
    t.integer  "node_level",    limit: 4
    t.integer  "orden",         limit: 4
    t.boolean  "sistema"
    t.boolean  "visible"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "tree_modified"
  end

  create_table "ciudads", force: :cascade do |t|
    t.string   "nombre",       limit: 4000
    t.string   "nombre_corto", limit: 4000
    t.integer  "estado_id",    limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "cliente_elite_forma_pagos", force: :cascade do |t|
    t.integer  "cliente_elite_id",    limit: 4
    t.string   "forma_pago",          limit: 4000
    t.string   "numero_tarjeta",      limit: 4000
    t.date     "vencimiento"
    t.string   "codigo_verificacion", limit: 4000
    t.string   "nombre_tarjeta",      limit: 4000
    t.string   "direccion",           limit: 4000
    t.integer  "ciudad_id",           limit: 4
    t.integer  "estado_id",           limit: 4
    t.integer  "codigo_postal",       limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "cliente_elite_forma_pagos", ["ciudad_id"], name: "index_cliente_elite_forma_pagos_on_ciudad_id"
  add_index "cliente_elite_forma_pagos", ["cliente_elite_id"], name: "index_cliente_elite_forma_pagos_on_cliente_elite_id"
  add_index "cliente_elite_forma_pagos", ["estado_id"], name: "index_cliente_elite_forma_pagos_on_estado_id"

  create_table "cliente_elite_productos", force: :cascade do |t|
    t.integer  "cliente_elite_id", limit: 4
    t.integer  "producto_id",      limit: 4
    t.integer  "cantidad",         limit: 4
    t.string   "clave",            limit: 4000
    t.float    "puntos"
    t.float    "precio_publico"
    t.float    "precio_cliente"
    t.string   "total",            limit: 4000
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "cliente_elite_productos", ["cliente_elite_id"], name: "index_cliente_elite_productos_on_cliente_elite_id"
  add_index "cliente_elite_productos", ["producto_id"], name: "index_cliente_elite_productos_on_producto_id"

  create_table "cliente_elites", force: :cascade do |t|
    t.string   "numero",                limit: 4000
    t.string   "apellido_paterno",      limit: 4000
    t.string   "apellido_materno",      limit: 4000
    t.string   "nombre",                limit: 4000
    t.date     "fecha_nacimiento"
    t.string   "domicilio_particular",  limit: 4000
    t.string   "colonia",               limit: 4000
    t.integer  "codigo_postal",         limit: 4
    t.string   "identificacion",        limit: 4000
    t.integer  "estado_donde_nacio_id", limit: 4
    t.string   "sexo",                  limit: 4000
    t.string   "telefono_particular",   limit: 4000
    t.string   "telefono_celular",      limit: 4000
    t.string   "email",                 limit: 4000
    t.integer  "ciudad_id",             limit: 4
    t.integer  "estado_id",             limit: 4
    t.integer  "pais_id",               limit: 4
    t.string   "rfc",                   limit: 4000
    t.string   "curp",                  limit: 4000
    t.date     "fecha_de_ingreso"
    t.integer  "socio_id",              limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "cliente_elites", ["ciudad_id"], name: "index_cliente_elites_on_ciudad_id"
  add_index "cliente_elites", ["estado_id"], name: "index_cliente_elites_on_estado_id"
  add_index "cliente_elites", ["socio_id"], name: "index_cliente_elites_on_socio_id"

  create_table "conceptos", force: :cascade do |t|
    t.string   "clave",       limit: 4000
    t.string   "concepto",    limit: 4000
    t.text     "descripcion", limit: 2147483647
    t.string   "imagen",      limit: 4000
    t.integer  "category_id", limit: 4
    t.boolean  "activo"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "conceptos", ["category_id"], name: "index_conceptos_on_category_id"

  create_table "estados", force: :cascade do |t|
    t.string   "estado",     limit: 4000
    t.string   "clave",      limit: 4000
    t.integer  "pais_id",    limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "factura_conceptos", force: :cascade do |t|
    t.integer  "factura_id",        limit: 4
    t.string   "no_identificacion", limit: 4000
    t.string   "descripcion",       limit: 4000
    t.float    "cantidad"
    t.string   "unidad",            limit: 4000
    t.float    "valor_unitario"
    t.float    "importe"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "factura_conceptos", ["factura_id"], name: "index_factura_conceptos_on_factura_id"

  create_table "facturas", force: :cascade do |t|
    t.string   "folio",        limit: 4000
    t.date     "fecha"
    t.string   "tipo_factura", limit: 4000
    t.integer  "proveedor_id", limit: 4
    t.float    "sub_total"
    t.float    "iva"
    t.float    "total"
    t.integer  "usuario_id",   limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "facturas", ["proveedor_id"], name: "index_facturas_on_proveedor_id"
  add_index "facturas", ["usuario_id"], name: "index_facturas_on_usuario_id"

  create_table "menus", force: :cascade do |t|
    t.string   "option",          limit: 4000
    t.string   "hint",            limit: 4000
    t.string   "shortcut",        limit: 4000
    t.integer  "menu_type",       limit: 4
    t.string   "url",             limit: 4000
    t.integer  "parent_id",       limit: 4
    t.string   "tree_path",       limit: 4000
    t.boolean  "has_child"
    t.integer  "node_level",      limit: 4
    t.integer  "iorder",          limit: 4
    t.boolean  "activo"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "tipo_permiso_id", limit: 4
  end

  add_index "menus", ["parent_id"], name: "index_menu_on_parent_id"
  add_index "menus", ["tree_path"], name: "index_menu_on_tree_path"

  create_table "pago_conceptos", force: :cascade do |t|
    t.integer  "pago_id",     limit: 4
    t.integer  "concepto_id", limit: 4
    t.float    "monto"
    t.integer  "order",       limit: 4
    t.string   "check_sum",   limit: 4000
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "pago_conceptos", ["concepto_id"], name: "index_pago_conceptos_on_concepto_id"
  add_index "pago_conceptos", ["pago_id"], name: "index_pago_conceptos_on_pago_id"

  create_table "pagos", force: :cascade do |t|
    t.string   "folio",        limit: 4000
    t.datetime "fecha"
    t.float    "monto"
    t.integer  "categoria_id", limit: 4
    t.integer  "usuario_id",   limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "concepto",     limit: 4000
  end

  add_index "pagos", ["usuario_id"], name: "index_pagos_on_usuario_id"

  create_table "paises", force: :cascade do |t|
    t.string   "pais",       limit: 4000
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "permisos", force: :cascade do |t|
    t.string   "descripcion",     limit: 4000
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "role_id",         limit: 4
    t.boolean  "can_see_index"
    t.boolean  "can_show"
    t.boolean  "can_create_new"
    t.boolean  "can_edit"
    t.boolean  "can_delete"
    t.boolean  "can_print"
    t.boolean  "can_export"
    t.integer  "tipo_permiso_id", limit: 4
  end

  add_index "permisos", ["role_id"], name: "index_permisos_on_role_id"

  create_table "plan_de_negocios", force: :cascade do |t|
    t.date     "fecha"
    t.integer  "socio_id",   limit: 4
    t.integer  "folio",      limit: 4
    t.float    "monto"
    t.integer  "nivel",      limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "plan_de_negocios", ["socio_id"], name: "index_plan_de_negocios_on_socio_id"

  create_table "precios", force: :cascade do |t|
    t.date     "fecha"
    t.integer  "category_id",        limit: 4
    t.integer  "concepto_id",        limit: 4
    t.float    "precio_normal"
    t.float    "precio_pronto_pago"
    t.date     "fecha_pronto_pago"
    t.date     "vigencia_de"
    t.date     "vigencia_hasta"
    t.boolean  "activo"
    t.integer  "usuario_id",         limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "precios", ["category_id"], name: "index_precios_on_category_id"
  add_index "precios", ["concepto_id"], name: "index_precios_on_concepto_id"
  add_index "precios", ["usuario_id"], name: "index_precios_on_usuario_id"

  create_table "productos", force: :cascade do |t|
    t.string   "clave",            limit: 4000
    t.string   "descripcion",      limit: 4000
    t.float    "costo"
    t.float    "precio"
    t.float    "precio_publico"
    t.float    "impuesto"
    t.float    "puntos"
    t.float    "puntos_calificar"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "proveedors", force: :cascade do |t|
    t.string   "nombre",          limit: 4000
    t.string   "rfc",             limit: 4000
    t.string   "domicilio",       limit: 4000
    t.string   "numero_interior", limit: 4000
    t.string   "numero_exterior", limit: 4000
    t.integer  "ciudad_id",       limit: 4
    t.integer  "estado_id",       limit: 4
    t.integer  "codigo_postal",   limit: 4
    t.string   "telefono1",       limit: 4000
    t.string   "telefono2",       limit: 4000
    t.string   "telefono3",       limit: 4000
    t.string   "contacto",        limit: 4000
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "proveedors", ["ciudad_id"], name: "index_proveedors_on_ciudad_id"
  add_index "proveedors", ["estado_id"], name: "index_proveedors_on_estado_id"

  create_table "roles", force: :cascade do |t|
    t.string   "role_name",       limit: 4000
    t.integer  "tipo_empresa_id", limit: 4
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "socio_forma_pagos", force: :cascade do |t|
    t.integer  "socio_id",            limit: 4
    t.string   "forma_pago",          limit: 4000
    t.string   "numero_tarjeta",      limit: 4000
    t.date     "vencimiento"
    t.string   "codigo_verificacion", limit: 4000
    t.string   "nombre_tarjeta",      limit: 4000
    t.string   "direccion",           limit: 4000
    t.integer  "ciudad_id",           limit: 4
    t.integer  "estado_id",           limit: 4
    t.integer  "codigo_postal",       limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "socio_forma_pagos", ["ciudad_id"], name: "index_socio_forma_pagos_on_ciudad_id"
  add_index "socio_forma_pagos", ["estado_id"], name: "index_socio_forma_pagos_on_estado_id"
  add_index "socio_forma_pagos", ["socio_id"], name: "index_socio_forma_pagos_on_socio_id"

  create_table "socio_productos", force: :cascade do |t|
    t.integer  "producto_id",    limit: 4
    t.integer  "cantidad",       limit: 4
    t.string   "clave",          limit: 4000
    t.float    "puntos"
    t.float    "precio_publico"
    t.float    "precio_socio"
    t.float    "total"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "socio_id",       limit: 4
  end

  add_index "socio_productos", ["producto_id"], name: "index_socio_productos_on_producto_id"

  create_table "socios", force: :cascade do |t|
    t.string   "numero",                      limit: 4000
    t.integer  "banco_id",                    limit: 4
    t.string   "cuenta",                      limit: 4000
    t.string   "tarjeta",                     limit: 4000
    t.string   "apellido_paterno",            limit: 4000
    t.string   "apellido_materno",            limit: 4000
    t.string   "nombre",                      limit: 4000
    t.date     "fecha_nacimiento"
    t.string   "domicilio_particular",        limit: 4000
    t.string   "colonia",                     limit: 4000
    t.integer  "codigo_postal",               limit: 4
    t.string   "documento_de_identificacion", limit: 4000
    t.integer  "estado_nacimiento_id",        limit: 4
    t.string   "sexo",                        limit: 4000
    t.string   "telefono_particular",         limit: 4000
    t.string   "telefono_celular",            limit: 4000
    t.string   "email",                       limit: 4000
    t.integer  "ciudad_id",                   limit: 4
    t.integer  "estado_id",                   limit: 4
    t.integer  "pais_id",                     limit: 4
    t.string   "rfc",                         limit: 4000
    t.string   "curp",                        limit: 4000
    t.date     "fecha_de_ingreso"
    t.string   "nombre_co_titular",           limit: 4000
    t.date     "fecha_nacimiento_ec"
    t.string   "email_ct",                    limit: 4000
    t.string   "telefono_celular_ct",         limit: 4000
    t.integer  "parent_id",                   limit: 4
    t.string   "tree_path",                   limit: 4000
    t.boolean  "has_child"
    t.integer  "node_level",                  limit: 4
    t.boolean  "copia_ife"
    t.boolean  "copia_curp"
    t.boolean  "copia_rfc"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "socios", ["banco_id"], name: "index_socios_on_banco_id"
  add_index "socios", ["ciudad_id"], name: "index_socios_on_ciudad_id"
  add_index "socios", ["estado_id"], name: "index_socios_on_estado_id"
  add_index "socios", ["numero"], name: "index_socios_on_numero"
  add_index "socios", ["parent_id"], name: "index_socios_on_parent_id"
  add_index "socios", ["tree_path"], name: "index_socios_on_tree_path"

  create_table "tipo_permisos", force: :cascade do |t|
    t.string   "tipo_permiso", limit: 4000
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "usuario",                limit: 4000
    t.string   "password",               limit: 4000
    t.string   "nombre",                 limit: 4000
    t.string   "email",                  limit: 4000
    t.boolean  "activo"
    t.integer  "role_id",                limit: 4
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.string   "encrypted_password",     limit: 4000, default: "", null: false
    t.string   "reset_password_token",   limit: 4000
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,    default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 4000
    t.string   "last_sign_in_ip",        limit: 4000
    t.string   "id_cliente",             limit: 4000
    t.integer  "socio_id",               limit: 4
  end

  add_index "usuarios", ["email"], name: "index_usuarios_on_email", unique: true
  add_index "usuarios", ["nombre"], name: "index_usuarios_on_nombre"
  add_index "usuarios", ["socio_id"], name: "index_usuarios_on_socio_id"

  add_foreign_key "beneficiarios", "socios"
  add_foreign_key "cliente_elite_forma_pagos", "ciudads"
  add_foreign_key "cliente_elite_forma_pagos", "cliente_elites"
  add_foreign_key "cliente_elite_forma_pagos", "estados"
  add_foreign_key "cliente_elite_productos", "cliente_elites"
  add_foreign_key "cliente_elite_productos", "productos"
  add_foreign_key "cliente_elites", "ciudads"
  add_foreign_key "cliente_elites", "estados"
  add_foreign_key "cliente_elites", "socios"
  add_foreign_key "conceptos", "categories"
  add_foreign_key "factura_conceptos", "facturas"
  add_foreign_key "facturas", "proveedors"
  add_foreign_key "facturas", "usuarios"
  add_foreign_key "pago_conceptos", "conceptos"
  add_foreign_key "pago_conceptos", "pagos"
  add_foreign_key "pagos", "usuarios"
  add_foreign_key "permisos", "roles"
  add_foreign_key "plan_de_negocios", "socios"
  add_foreign_key "precios", "categories"
  add_foreign_key "precios", "conceptos"
  add_foreign_key "precios", "usuarios"
  add_foreign_key "proveedors", "ciudads"
  add_foreign_key "proveedors", "estados"
  add_foreign_key "socio_forma_pagos", "ciudads"
  add_foreign_key "socio_forma_pagos", "estados"
  add_foreign_key "socio_forma_pagos", "socios"
  add_foreign_key "socio_productos", "productos"
  add_foreign_key "socios", "bancos"
  add_foreign_key "socios", "ciudads"
  add_foreign_key "socios", "estados"
  add_foreign_key "usuarios", "socios"
end
