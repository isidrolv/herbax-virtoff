class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :usuario
      t.string :password
      t.string :nombre
      t.string :email
      t.boolean :activo
      t.integer :role_id

      t.timestamps null: false
    end
  end
end
