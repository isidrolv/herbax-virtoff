class CreateProveedors < ActiveRecord::Migration
  def change
    create_table :proveedors do |t|
      t.string :nombre
      t.string :rfc
      t.string :domicilio
      t.string :numero_interior
      t.string :numero_exterior
      t.references :ciudad, index: true, foreign_key: true
      t.references :estado, index: true, foreign_key: true
      t.integer :codigo_postal
      t.string :telefono1
      t.string :telefono2
      t.string :telefono3
      t.string :contacto

      t.timestamps null: false
    end
  end
end
