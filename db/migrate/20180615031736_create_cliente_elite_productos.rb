class CreateClienteEliteProductos < ActiveRecord::Migration
  def change
    create_table :cliente_elite_productos do |t|
      t.references :cliente_elite, index: true, foreign_key: true
      t.references :producto, index: true, foreign_key: true
      t.integer :cantidad
      t.string :clave
      t.float :puntos
      t.float :precio_publico
      t.float :precio_cliente
      t.string :total

      t.timestamps null: false
    end
  end
end
