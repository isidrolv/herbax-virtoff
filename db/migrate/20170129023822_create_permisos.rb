class CreatePermisos < ActiveRecord::Migration
  def change
    create_table :permisos do |t|
      t.string :permiso
      t.string :descripcion
      t.integer :tipo_accion
      t.integer :parent_id
      t.string :treepath
      t.integer :node_level
      t.boolean :has_child
      t.integer :iorder
      t.string :object

      t.timestamps null: false
    end
  end
end
