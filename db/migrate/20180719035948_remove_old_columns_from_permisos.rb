class RemoveOldColumnsFromPermisos < ActiveRecord::Migration
  def change
    remove_column :permisos, :permiso, :string
    remove_column :permisos, :tipo_accion, :integer
    remove_column :permisos, :parent_id, :integer
    remove_column :permisos, :treepath, :string
    remove_column :permisos, :node_level, :integer
    remove_column :permisos, :has_child, :boolean
    remove_column :permisos, :iorder, :integer
    remove_column :permisos, :object, :string
  end
end
