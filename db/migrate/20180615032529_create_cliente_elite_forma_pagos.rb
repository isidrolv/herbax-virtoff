class CreateClienteEliteFormaPagos < ActiveRecord::Migration
  def change
    create_table :cliente_elite_forma_pagos do |t|
      t.references :cliente_elite, index: true, foreign_key: true
      t.string :forma_pago
      t.string :numero_tarjeta
      t.date :vencimiento
      t.string :codigo_verificacion
      t.string :nombre_tarjeta
      t.string :direccion
      t.references :ciudad, index: true, foreign_key: true
      t.references :estado, index: true, foreign_key: true
      t.integer :codigo_postal

      t.timestamps null: false
    end
  end
end
