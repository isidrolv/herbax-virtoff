class CreateCiudads < ActiveRecord::Migration
  def change
    create_table :ciudads do |t|
      t.string :nombre
      t.string :nombre_corto
      t.integer :estado_id

      t.timestamps null: false
    end
  end
end
