class CreateFacturaConceptos < ActiveRecord::Migration
  def change
    create_table :factura_conceptos do |t|
      t.references :factura, index: true, foreign_key: true
      t.string :no_identificacion
      t.string :descripcion
      t.float :cantidad
      t.string :unidad
      t.float :valor_unitario
      t.float :importe

      t.timestamps null: false
    end
  end
end
