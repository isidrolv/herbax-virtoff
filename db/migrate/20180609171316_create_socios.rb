class CreateSocios < ActiveRecord::Migration
  def change
    create_table :socios do |t|
      t.string :numero, index: true
      t.references :banco, index: true, foreign_key: true
      t.string :cuenta
      t.string :tarjeta
      t.string :apellido_paterno
      t.string :apellido_materno
      t.string :nombre
      t.date :fecha_nacimiento
      t.string :domicilio_particular
      t.string :colonia
      t.integer :codigo_postal
      t.string :documento_de_identificacion
      t.integer :estado_nacimiento_id
      t.string :sexo
      t.string :telefono_particular
      t.string :telefono_celular
      t.string :email
      t.references :ciudad, index: true, foreign_key: true
      t.references :estado, index: true, foreign_key: true
      t.integer :pais_id
      t.string :rfc
      t.string :curp
      t.date :fecha_de_ingreso
      t.string :nombre_co_titular
      t.date :fecha_nacimiento_ec
      t.string :email_ct
      t.string :telefono_celular_ct
      t.integer :parent_id
      t.string :tree_path
      t.boolean :has_child
      t.integer :node_level
      t.boolean :copia_ife
      t.boolean :copia_curp
      t.boolean :copia_rfc

      t.timestamps null: false
    end

    add_index :socios, [:nombre, :apellido_paterno, :apellido_materno], name: "index_on_nombre_paterno_materno"
    add_index :socios, [:apellido_paterno, :apellido_materno, :nombre], name: "index_on_paterno_materno_nombre"

  end
end
