class CreateClienteElites < ActiveRecord::Migration
  def change
    create_table :cliente_elites do |t|
      t.string :numero
      t.string :apellido_paterno
      t.string :apellido_materno
      t.string :nombre
      t.date :fecha_nacimiento
      t.string :domicilio_particular
      t.string :colonia
      t.integer :codigo_postal
      t.string :identificacion
      t.integer :estado_donde_nacio_id
      t.string :sexo
      t.string :telefono_particular
      t.string :telefono_celular
      t.string :email
      t.references :ciudad, index: true, foreign_key: true
      t.references :estado, index: true, foreign_key: true
      t.integer :pais_id
      t.string :rfc
      t.string :curp
      t.date :fecha_de_ingreso
      t.references :socio, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
