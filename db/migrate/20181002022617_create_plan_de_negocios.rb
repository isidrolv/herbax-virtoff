class CreatePlanDeNegocios < ActiveRecord::Migration
  def change
    create_table :plan_de_negocios do |t|
      t.date :fecha
      t.references :socio, index: true, foreign_key: true
      t.integer :folio
      t.float :monto
      t.integer :nivel

      t.timestamps null: false
    end
  end
end
