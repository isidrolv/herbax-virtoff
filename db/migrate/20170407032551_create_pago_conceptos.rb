class CreatePagoConceptos < ActiveRecord::Migration
  def change
    create_table :pago_conceptos do |t|
      t.references :pago, index: true, foreign_key: true
      t.references :concepto, index: true, foreign_key: true
      t.float :monto
      t.integer :order
      t.string :check_sum

      t.timestamps null: false
    end
  end
end
