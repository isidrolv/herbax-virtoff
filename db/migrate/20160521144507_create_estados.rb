class CreateEstados < ActiveRecord::Migration
  def change
    create_table :estados do |t|
      t.string :estado
      t.string :clave
      t.integer :pais_id

      t.timestamps null: false
    end
  end
end
