class CreatePagos < ActiveRecord::Migration
  def change
    create_table :pagos do |t|
      t.string :folio
      t.datetime :fecha
      t.float :monto
      t.integer :categoria_id
      t.references :usuario, index: true, foreign_key: true
      t.references :alumno, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
