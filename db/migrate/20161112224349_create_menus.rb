class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :option
      t.string :hint
      t.string :shortcut
      t.integer :menu_type
      t.string :url
      t.integer :permiso_id
      t.integer :parent_id
      t.string :tree_path
      t.boolean :has_child
      t.integer :node_level
      t.integer :iorder
      t.boolean :activo

      t.timestamps null: false
    end
  end
end
