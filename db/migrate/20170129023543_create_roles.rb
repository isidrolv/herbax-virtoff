class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string :role_name
      t.integer :tipo_empresa_id

      t.timestamps null: false
    end
  end
end
