class AddIndexToUsuariosByNombre < ActiveRecord::Migration
  def change
    add_index :usuarios, :nombre
  end
end
