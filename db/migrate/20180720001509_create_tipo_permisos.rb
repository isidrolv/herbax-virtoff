class CreateTipoPermisos < ActiveRecord::Migration
  def change
    create_table :tipo_permisos do |t|
      t.string :tipo_permiso

      t.timestamps null: false
    end
  end
end
