class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :categoria
      t.integer :val1
      t.integer :val2
      t.string :vals1
      t.string :vals2
      t.integer :parent_id
      t.string :tree_path
      t.boolean :activo
      t.boolean :has_child
      t.integer :node_level
      t.integer :orden
      t.boolean :sistema
      t.boolean :visible

      t.timestamps null: false
    end
  end
end
