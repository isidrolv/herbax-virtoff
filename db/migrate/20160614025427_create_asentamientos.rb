class CreateAsentamientos < ActiveRecord::Migration
  def change
    create_table :asentamientos do |t|
      t.string :nombre
      t.integer :tipo_asentamiento_id
      t.integer :codigo_postal
      t.integer :ciudad_id

      t.timestamps null: false
    end
  end
end
