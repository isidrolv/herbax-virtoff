class AddTreeModifiedFieldToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :tree_modified, :boolean
  end
end
