class CreateBeneficiarios < ActiveRecord::Migration
  def change
    create_table :beneficiarios do |t|
      t.references :socio, index: true, foreign_key: true
      t.string :nombre
      t.float :porcentaje

      t.timestamps null: false
    end
  end
end
