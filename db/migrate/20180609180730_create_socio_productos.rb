class CreateSocioProductos < ActiveRecord::Migration
  def change
    create_table :socio_productos do |t|
      t.references :producto, index: true, foreign_key: true
      t.integer :cantidad
      t.string :clave
      t.float :puntos
      t.float :precio_publico
      t.float :precio_socio
      t.float :total

      t.timestamps null: false
    end
  end
end
