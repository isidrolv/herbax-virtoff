class CreatePrecios < ActiveRecord::Migration
  def change
    create_table :precios do |t|
      t.date :fecha
      t.references :category, index: true, foreign_key: true
      t.references :concepto, index: true, foreign_key: true
      t.float :precio_normal
      t.float :precio_pronto_pago
      t.date :fecha_pronto_pago
      t.date :vigencia_de
      t.date :vigencia_hasta
      t.boolean :activo
      t.references :usuario, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
