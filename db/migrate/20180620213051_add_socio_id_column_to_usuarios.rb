class AddSocioIdColumnToUsuarios < ActiveRecord::Migration
  def change
    add_reference :usuarios, :socio, index: true, foreign_key: true
  end
end
