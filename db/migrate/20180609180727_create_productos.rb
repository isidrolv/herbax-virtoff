class CreateProductos < ActiveRecord::Migration
  def change
    create_table :productos do |t|
      t.string :clave
      t.string :descripcion
      t.float :costo
      t.float :precio
      t.float :precio_publico
      t.float :impuesto
      t.float :puntos
      t.float :puntos_calificar

      t.timestamps null: false
    end
  end
end
