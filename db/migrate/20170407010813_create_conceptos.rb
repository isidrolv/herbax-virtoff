class CreateConceptos < ActiveRecord::Migration
  def change
    create_table :conceptos do |t|
      t.string :clave
      t.string :concepto
      t.text :descripcion
      t.string :imagen
      t.references :category, index: true, foreign_key: true
      t.boolean :activo

      t.timestamps null: false
    end
  end
end
