class AddNewColumnsToPermisos < ActiveRecord::Migration
  def change
    add_reference :permisos, :role, index: true, foreign_key: true
    add_column :permisos, :can_see_index, :boolean
    add_column :permisos, :can_show, :boolean
    add_column :permisos, :can_create_new, :boolean
    add_column :permisos, :can_edit, :boolean
    add_column :permisos, :can_delete, :boolean
    add_column :permisos, :can_print, :boolean
    add_column :permisos, :can_export, :boolean
  end
end
