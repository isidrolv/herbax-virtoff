class CreateSocioFormaPagos < ActiveRecord::Migration
  def change
    create_table :socio_forma_pagos do |t|
      t.references :socio, index: true, foreign_key: true
      t.string :forma_pago
      t.string :numero_tarjeta
      t.date :vencimiento
      t.string :codigo_verificacion
      t.string :nombre_tarjeta
      t.string :direccion
      t.references :ciudad, index: true, foreign_key: true
      t.references :estado, index: true, foreign_key: true
      t.integer :codigo_postal

      t.timestamps null: false
    end
  end
end
