class RemovePermisoIdFromMenus < ActiveRecord::Migration
  def change
    remove_column :menus, :permiso_id, :string
  end
end
