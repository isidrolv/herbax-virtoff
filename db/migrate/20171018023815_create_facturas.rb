class CreateFacturas < ActiveRecord::Migration
  def change
    create_table :facturas do |t|
      t.string :folio
      t.date :fecha
      t.string :tipo_factura
      t.references :proveedor, index: true, foreign_key: true
      t.float :sub_total
      t.float :iva
      t.float :total
      t.references :usuario, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
