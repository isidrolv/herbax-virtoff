n = 0
[["Alumnos","#"],
["Datos Personales","/alumnos"],
["Expediente","/expediente"],
["Maestros","#"],
["Datos personales","/maestros"],
["Academias","/academias"],
["Reportes","/reportes_de_maestros"],
["Control Academico","#"],
["Cursos","/cursos"],
["Inscripciones","/inscripcions"],
["Calificaciones","/calificacions"],
["Kardex","/kardex"],
["Becas","/becas"],
["Tesorería","#"],
["Pagos","/pagos"],
["Catalogos","#"],
["Categorias","/categories"],
["Tipos de Cursos","/tipo_cursos"],
["Ciudades","/ciudads"],
["Estados","/estados"],
["Paises","/paises"],
["Asentamientos","/asentamientos"],
["--","#"],
["SysAdmin","#"],
["Seguridad","#"],
["Usuarios","/usuarios"],
["Roles y Permisos","/roles"],
["Auditoria","#"]].each do | menu |
	n += 1
	puts "{option: '#{menu[0]}', hint: '#{menu[0]}', shortcut: 'Ctrl-U', menu_type: 1, url: '#{menu[1]}', permiso_id: nil, parent_id: 1, iorder: #{n}, activo: true},"

end