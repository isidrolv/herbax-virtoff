class MenuLoader

  def self.load
    Menu.delete_all
    #Menu.connection.execute "delete from sqlite_sequence where name='menus';" #solo para sqlite
    Menu.connection.execute 'ALTER TABLE menus AUTO_INCREMENT = 1;' # para mysql 
    Menu.create!([
      {option: 'Alumnos', hint: 'Módulo de alumnos', shortcut: 'Ctrl-A', menu_type: 1, url: '#', permiso_id: -1, tree_path: '00001', has_child: true, node_level: 1, iorder: nil, activo: true}, 
      {option: 'Datos Personales', hint: 'Datos personales de Alumnos', shortcut: 'Ctrl-D, Ctrl-A', menu_type: 1, url: '/alumnos', permiso_id: -1, tree_path: '00001.00002', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Expediente', hint: 'Expediente académico de Alumnos', shortcut: 'Ctrl-P, Ctrl-A', menu_type: 1, url: '/expediente', permiso_id: -1, tree_path: '00001.00003', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Calificaciones', hint: 'Control de Calificaciones de Alumnos', shortcut: 'Ctrl-K, Ctrl-A', menu_type: 1, url: '/calificacions', permiso_id: -1, tree_path: '00001.00006', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Maestros', hint: 'Control de Maestros', shortcut: 'Ctrl-M, Ctrl-A', menu_type: 1, url: '#', permiso_id: -1, tree_path: '00008', has_child: true, node_level: 1, iorder: nil, activo: true}, 
      {option: 'Datos personales', hint: 'Datos personales de Maestros', shortcut: 'Ctrl-M, Ctrl-D', menu_type: 1, url: '/maestros', permiso_id: -1, tree_path: '00008.00009', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Academias', hint: 'Reportes de Maestros', shortcut: 'Ctrl-M, Ctrl-D', menu_type: 1, url: '/reportes_de_maestros', permiso_id: -1, tree_path: '00008.00010', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Reportes', hint: 'Reportes de Maestros', shortcut: 'Ctrl-M, Ctrl-D', menu_type: 1, url: '/reportes_de_maestros', permiso_id: -1, tree_path: '00008.00011', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Control Academico', hint: 'Control Academico', shortcut: 'Ctrl-M, Ctrl-D', menu_type: 1, url: '#', permiso_id: -1, tree_path: '00012', has_child: true, node_level: 1, iorder: nil, activo: true}, 
      {option: 'Inscripciones', hint: 'Control de Inscripciones de Alumnos', shortcut: 'Ctrl-A, Ctrl-I', menu_type: 1, url: '/inscripcions', permiso_id: -1, tree_path: '00012.00005', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Becas', hint: 'Control de Becas de Alumnos', shortcut: 'Ctrl-B, Ctrl-A', menu_type: 1, url: '/becas', permiso_id: -1, tree_path: '00012.00007', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Cursos', hint: 'Control de Cursos', shortcut: 'Ctrl-Shift-C', menu_type: 1, url: '/cursos', permiso_id: -1, tree_path: '00012.00013', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Kardex', hint: nil, shortcut: nil, menu_type: nil, url: '/kardex', permiso_id: nil, tree_path: '00012.00024', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Tesorería', hint: nil, shortcut: nil, menu_type: nil, url: '#', permiso_id: nil, tree_path: '00023', has_child: true, node_level: 1, iorder: nil, activo: true}, 
      {option: 'Pagos', hint: 'Control de Pagos de Alumnos', shortcut: 'Ctrl-P, Ctrl-P', menu_type: 1, url: '/pagos', permiso_id: -1, tree_path: '00023.00004', has_child: false, node_level: 2, iorder: nil, activo: true},
      {option: 'Catalogos', hint: 'Catalogos del Sistema', shortcut: 'Ctrl-Shift-C', menu_type: 1, url: '#', permiso_id: -1, tree_path: '00014', has_child: true, node_level: 1, iorder: nil, activo: true}, 
      {option: 'Categorias', hint: 'Categorias del Sistema', shortcut: 'Ctrl-Shift-C', menu_type: 1, url: '/categories', permiso_id: -1, tree_path: '00014.00015', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Tipos de Cursos', hint: 'Catalogo de Tipos de Cursos', shortcut: 'Ctrl-Shift-C', menu_type: 1, url: '/tipo_cursos', permiso_id: -1, tree_path: '00014.00016', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Ciudades', hint: 'Catalogo de Ciudades', shortcut: 'Ctrl-Shift-C', menu_type: 1, url: '/ciudads', permiso_id: -1, tree_path: '00014.00017', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Estados', hint: 'Catalogo de Estados', shortcut: 'Ctrl-Shift-C', menu_type: 1, url: '/estados', permiso_id: -1, tree_path: '00014.00018', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Paises', hint: 'Catalogo de Paises', shortcut: 'Ctrl-Shift-C', menu_type: 1, url: '/paises', permiso_id: -1, tree_path: '00014.00019', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Asentamientos', hint: 'Catalogo de Asentamientos', shortcut: 'Ctrl-Shift-C', menu_type: 1, url: '/asentamientos', permiso_id: -1, tree_path: '00014.00020', has_child: false, node_level: 2, iorder: nil, activo: true}, 
      {option: 'SysAdmin', hint: nil, shortcut: nil, menu_type: nil, url: '#', permiso_id: nil, tree_path: '00014.00025', has_child: true, node_level: 2, iorder: nil, activo: true}, 
      {option: 'Seguridad', hint: nil, shortcut: nil, menu_type: nil, url: '#', permiso_id: nil, tree_path: '00014.00025.00026', has_child: true, node_level: 3, iorder: nil, activo: true}, 
      {option: 'Usuarios', hint: 'Usuarios del Sistema', shortcut: 'Ctrl-Shift-C', menu_type: 1, url: '/usuarios', permiso_id: -1, tree_path: '00014.00025.00026.00021', has_child: false, node_level: 4, iorder: nil, activo: true}, 
      {option: 'Roles y Permisos', hint: 'Roles y permisos de los Usuarios del Sistema', shortcut: 'Ctrl-Shift-C', menu_type: 1, url: '/roles', permiso_id: -1, tree_path: '00014.00025.00026.00022', has_child: false, node_level: 4, iorder: nil, activo: true}, 
      {option: '--', hint: nil, shortcut: nil, menu_type: nil, url: '#', permiso_id: nil, tree_path: '00014.00025.00027', has_child: false, node_level: 3, iorder: nil, activo: true}, 
      {option: 'Auditoria', hint: nil, shortcut: nil, menu_type: nil, url: '#', permiso_id: nil, tree_path: '00014.00025.00028', has_child: false, node_level: 3, iorder: nil, activo: true}, 
     ])

    Menu.find(1).update_attributes parent_id: nil
    Menu.find(2).update_attributes parent_id: 1
    Menu.find(3).update_attributes parent_id: 1
    Menu.find(4).update_attributes parent_id: 1
    Menu.find(5).update_attributes parent_id: nil
    Menu.find(6).update_attributes parent_id: 5
    Menu.find(7).update_attributes parent_id: 5
    Menu.find(8).update_attributes parent_id: 5
    Menu.find(9).update_attributes parent_id: nil
    Menu.find(10).update_attributes parent_id: 9
    Menu.find(11).update_attributes parent_id: 9
    Menu.find(12).update_attributes parent_id: 9
    Menu.find(13).update_attributes parent_id: 9
    Menu.find(14).update_attributes parent_id: nil
    Menu.find(15).update_attributes parent_id: 14
    Menu.find(16).update_attributes parent_id: nil
    Menu.find(17).update_attributes parent_id: 16
    Menu.find(18).update_attributes parent_id: 16
    Menu.find(19).update_attributes parent_id: 16
    Menu.find(20).update_attributes parent_id: 16
    Menu.find(21).update_attributes parent_id: 16
    Menu.find(22).update_attributes parent_id: 16
    Menu.find(23).update_attributes parent_id: 16
    Menu.find(24).update_attributes parent_id: 23
    Menu.find(25).update_attributes parent_id: 24
    Menu.find(26).update_attributes parent_id: 24
    Menu.find(27).update_attributes parent_id: 23
    Menu.find(26).update_attributes parent_id: 23
    Menu.all.each{|m| m.update_attributes tree_path: "upd"}
  end

end
