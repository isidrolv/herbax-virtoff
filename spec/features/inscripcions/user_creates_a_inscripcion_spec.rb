# spec/features/categories/user_creates_a_inscripcion_spec.rb

feature 'User creates a new inscripcion' do

  scenario 'user creates a new simple inscripcion' do
    visit new_inscripcion_path
    fill_in 'ìnscriopcion_alumno_id', with: 1
    fill_in 'ìnscriopcion_fecha', with: 'Apr 05, 2017'
    click_button 'Guardar'
    expect(page).to have_content 'inscripcion ha sido guardada exitósamente'
  end

  scenario 'Check for the message labels in new simple inscripcion screen' do
    visit new_inscripcion_path
    expect(page).to have_content 'Por favor seleccione'
  end

end