# spec/features/categories/user_creates_a_category_spec.rb

feature 'User creates a new category' do

  scenario 'user creates a new simple root category' do
    visit new_category_path
    fill_in 'category_categoria', with: 'My new category'
    click_button 'Guardar'
    expect(page).to have_content 'My new category'
  end

  scenario 'user creates a new simple child category' do
    visit new_category_path
    fill_in 'category_categoria', with: 'My new category'
    click_button 'Guardar'

    visit new_category_path
    fill_in 'category_categoria', with: 'My new child category'
    select 'My new category', from: 'category_parent_id'
    click_button 'Guardar'
    expect(page).to have_content 'My new child category'
  end

  scenario 'user creates two new child categories' do
    visit new_category_path
    fill_in 'category_categoria', with: 'My new category'
    click_button 'Guardar'
    expect(page).to have_content 'My new category'

    visit new_category_path
    fill_in 'category_categoria', with: 'My new child category'
    select 'My new category', from: 'category_parent_id'
    click_button 'Guardar'
    expect(page).to have_content 'My new child category'

    visit new_category_path
    fill_in 'category_categoria', with: 'My second child category'
    select 'My new category', from: 'category_parent_id'
    click_button 'Guardar'
    expect(page).to have_content 'My second child category'
  end


end