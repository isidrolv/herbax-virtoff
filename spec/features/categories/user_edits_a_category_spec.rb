# spec/features/categories/user_edits_a_category_spec.rb

feature 'User edits an existing category' do

  scenario 'user edits a simple root category with no children' do
    visit new_category_path
    fill_in 'category_categoria', with: 'My new category'
    click_button 'Guardar'

    visit edit_category_path({id: 1})
    fill_in 'category_categoria', with: 'My category is modified'
    click_button 'Guardar'
    expect(page).to have_content 'My category is modified'
  end

  scenario 'user edits a new simple child category' do
    visit new_category_path
    fill_in 'category_categoria', with: 'My new category'
    click_button 'Guardar'

    visit new_category_path
    fill_in 'category_categoria', with: 'My new child category'
    select 'My new category', from: 'category_parent_id'
    click_button 'Guardar'
    expect(page).to have_content 'My new child category'
  end

  scenario 'user edits two new child categories' do
    visit new_category_path
    fill_in 'category_categoria', with: 'My new category'
    click_button 'Guardar'
    expect(page).to have_content 'My new category'

    visit new_category_path
    fill_in 'category_categoria', with: 'My new child category'
    select 'My new category', from: 'category_parent_id'
    click_button 'Guardar'
    expect(page).to have_content 'My new child category'

    visit new_category_path
    fill_in 'category_categoria', with: 'My second child category'
    select 'My new category', from: 'category_parent_id'
    click_button 'Guardar'
    expect(page).to have_content 'My second child category'
  end


end