# spec/features/alumnos/user_creates_an_alumno_spec.rb

feature 'User creates a new alumno' do

  scenario 'user creates a new simple alumno' do
    visit new_alumno_path
    fill_in 'alumno_matricula', with: '939393'
    fill_in 'alumno_nombre', with: 'My new alumno'
    fill_in 'alumno_paterno', with: 'My new alumno'
    fill_in 'alumno_materno', with: 'My new alumno'
    fill_in 'alumno_email', with: 'alumno@gmail.com'
    click_button 'Guardar'
    expect(page).to have_content 'Alumno ha sido dado de alta.'
  end

  scenario 'user tries to create a new simple alumno with incomplete data' do
    visit new_alumno_path
    fill_in 'alumno_matricula', with: 'M939393'
    fill_in 'alumno_nombre', with: 'My new alumno'
    click_button 'Guardar'
    expect(page).to have_content '2 errors evitan que sea guardada la informacion'
  end

  scenario 'user tries to create another simple alumno with incomplete data' do
    visit new_alumno_path
    fill_in 'alumno_matricula', with: '3939393'
    click_button 'Guardar'
    expect(page).to have_content '2 errors evitan que sea guardada la informacion'
  end

  scenario 'user tries to create another simple alumno with incomplete data' do
    visit new_alumno_path
    fill_in 'alumno_nombre', with: 'My new alumno'
    click_button 'Guardar'
    expect(page).to have_content '3 errors evitan que sea guardada la informacion'
  end


end