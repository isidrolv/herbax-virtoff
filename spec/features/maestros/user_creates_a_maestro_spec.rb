# spec/features/maestros/user_creates_a_maestro_spec.rb

feature 'User creates a new maestro' do

  scenario 'user creates a new simple maestro' do
    visit new_maestro_path
    fill_in 'maestro_clave', with: 'M939393'
    fill_in 'maestro_nombre', with: 'My new maestro'
    fill_in 'maestro_paterno', with: 'My new maestro'
    fill_in 'maestro_materno', with: 'My new maestro'
    click_button 'Guardar'
    expect(page).to have_content 'My new maestro'
  end

  scenario 'user tries to create a new simple maestro with incomplete data' do
    visit new_maestro_path
    fill_in 'maestro_clave', with: 'M939393'
    fill_in 'maestro_nombre', with: 'My new maestro'
    click_button 'Guardar'
    expect(page).to have_content '1 error evitan que sea guardada la informacion'
  end

  scenario 'user tries to create another simple maestro with incomplete data' do
    visit new_maestro_path
    fill_in 'maestro_clave', with: 'M939393'
    click_button 'Guardar'
    expect(page).to have_content '2 errors evitan que sea guardada la informacion'
  end

  scenario 'user tries to create another simple maestro with incomplete data' do
    visit new_maestro_path
    fill_in 'maestro_nombre', with: 'My new maestro'
    click_button 'Guardar'
    expect(page).to have_content '2 errors evitan que sea guardada la informacion'
  end


end