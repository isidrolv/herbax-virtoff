# Feature: 'Categorias' page
#   As a visitor
#   I want to visit an 'categorias' page
#   So I can see the list of categories
feature 'Validate menu presentation' do

  # Scenario: Visit the 'any page' page
  #   Given I am a visitor
  #   When I visit the 'See the menu' page
  #   Then I see "the list of categories on index page"
  scenario 'Visit the categorias index page' do
    visit '/'
    expect(page).to have_content 'Escolar'
    expect(page).to have_content 'Alumnos'
    expect(page).to have_content 'Maestros'
    expect(page).to have_content 'Control Academico'
  end

end
