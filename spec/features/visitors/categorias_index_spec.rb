# Feature: 'Categorias' page
#   As a visitor
#   I want to visit an 'categorias' page
#   So I can see the list of categories
feature 'Categorias page' do

  # Scenario: Visit the 'Categorias' page
  #   Given I am a visitor
  #   When I visit the 'categorias' page
  #   Then I see "the list of categories on index page"
  scenario 'Visit the categorias index page' do
    visit '/categories#index'
    expect(page).to have_content 'Categorias'
    expect(page).to have_content 'Nueva Categoria'
    expect(page).to have_content 'Categoria'
    expect(page).to have_content 'Acción'
  end

end
