require 'rails_helper'

RSpec.describe Menu, type: :model do

  context "operacion ABC de menus: " do 

    it "list all" do
      Menu.all.map { |e| e.option }
    end

    it "agrega nueva opcion al menu" do
      menu = Menu.create option: "Prueba", hint:"Hint prueba", shortcut: "shortcut prueba", menu_type: 1, url: "/prueba", permiso_id: 1, parent_id: nil, iorder: 1, activo: true  
      
      expect(menu.option).to eq "Prueba"
    end

     it "agrega menu sin datos" do 
       menu = FactoryBot.build(:menu, option: nil)
       expect(menu).not_to be_valid
     end

    # it "agrega menu sin matricula" do
    #   menu = FactoryBot.build(:menu, nombre: nil, paterno: nil)
    #   expect(menu).not_to be_valid
    # end
 
  end

end
