require 'rails_helper'

RSpec.describe Category, type: :model do
  
  context "categoria sin hijos: " do 

    it "list all" do
      Category.all.map { |e| e.categoria }
    end

    it "agrega nueva categoria" do
      raiz = Category.create categoria: "Raiz", has_child: false
      check_tree_node(raiz, false, 1, raiz.id.to_s.rjust(5,'0')    )
    end

    it "agrega dos categorias sin enlazar" do 
      raiz = Category.create categoria: "Categoria padre", has_child: false, node_level: 1
      hijo = Category.create categoria: "Categoria raiz tambien"
      check_tree_node(raiz, false, 1, raiz.id.to_s.rjust(5,'0'))
      check_tree_node(hijo, false, 1, hijo.id.to_s.rjust(5,'0')    )
    end
  end

  context "categoria con hijos: " do 

    it "agrega dos categorias enlazadas padre->hijo" do 
      raiz = Category.create categoria: "Categoria padre", has_child: false, node_level: 1
      hijo = Category.create categoria: "Categoria hija", parent_id: raiz.id
      raiz.reload

      #hijo.update_attributes tree_path: "kk"
      
      check_tree_node(raiz, true, 1, raiz.id.to_s.rjust(5,'0'))
      check_tree_node(hijo, false, 2, raiz.id.to_s.rjust(5,'0') + "." + hijo.id.to_s.rjust(5,'0') )
    end

    it "crear tres categorias enlazadas abuelo->padre->nieto" do 
      raiz = Category.create categoria: "Categoria abuela", has_child: false, node_level: 1
      hijo = Category.create categoria: "Categoria padre", parent_id: raiz.id
      nieto = Category.create categoria: "Categoria hija", parent_id: hijo.id
      raiz.reload
      hijo.reload

      #hijo.update_attributes tree_path: "kk"
      
      check_tree_node(raiz, true, 1, raiz.id.to_s.rjust(5,'0')    )
      check_tree_node(hijo, true, 2, raiz.id.to_s.rjust(5,'0') + "." + hijo.id.to_s.rjust(5,'0') )
      check_tree_node(nieto, false, 3, raiz.id.to_s.rjust(5,'0') + "." + hijo.id.to_s.rjust(5,'0') + "." + nieto.id.to_s.rjust(5,'0'))
    end
  end

  context "validar posibles conflictos con el arbol" do
    
    it "cambiar estado de un nodo hijo a nodo raiz" do
      raiz = Category.create categoria: "Categoria padre", has_child: false, node_level: 0
      hijo = Category.create categoria: "Categoria hija", parent_id: raiz.id
      raiz.reload
      
      p "validar parentela"
      check_tree_node(raiz, true, 1, raiz.id.to_s.rjust(5,'0'))
      check_tree_node(hijo, false, 2, raiz.id.to_s.rjust(5,'0') + "." + hijo.id.to_s.rjust(5,'0') )

      p "cambiar hijo a raiz"
      hijo.update_attributes parent_id: nil

      p """SUGERENCIA: Crear un UpdateTreeHelper.update_tree para evitar el conflicto 
          de nodo perdido donde se guarde el previo parent
          y se actualice despues de guardar el nodo actual."

      raiz.update_attributes tree_path: ""
      raiz.reload
      hijo.reload

      check_tree_node(hijo, false, 1, hijo.id.to_s.rjust(5,'0'))
      check_tree_node(raiz, false, 1, raiz.id.to_s.rjust(5,'0'))
    end

    it "Validar cuando se pone un hijo como padre" do
      
      raiz = Category.create categoria: "Categoria abuela", has_child: false, node_level: 1
      hijo = Category.create categoria: "Categoria padre", parent_id: raiz.id
      nieto = Category.create categoria: "Categoria hija", parent_id: raiz.id
      nieto2 = Category.create categoria: "Categoria hija2", parent_id: raiz.id

      p raiz.children

      expect {
        raiz.reload
        raiz.update_attributes parent_id: nieto.id      
      }.to raise_error("se esta usando un parent_id y ya es hijo")

    end

  end

  context "Validar category_tree" do
    
    it "mostrar el prompt adecuado para el nodo" do
      
      raiz = Category.create categoria: "Categoria padre", has_child: false, node_level: 0

      expect(raiz.category_tree).to eq "--->Categoria padre"

    end

  end


  context "Validar estructura hash para get_children" do
    
    it "mostrar el hash para arbol vacio" do

      hash = Category.get_children 1

      expect(hash).to eq []
    end

    it "mostrar el hash para arbol con un nodo" do
      raiz = Category.create categoria: "Categoria abuela", has_child: false, node_level: 1, activo: true

      hash = Category.get_children raiz.id

      expect(hash).to eq []

    end

    it "mostrar el hash para arbol con un padre y dos hijos" do
      raiz = Category.create categoria: "Categoria abuela", has_child: false, node_level: 1, activo: true
      hijo = Category.create categoria: "Categoria padre", parent_id: raiz.id, node_level: 1, activo: true
      hijo2 = Category.create categoria: "Categoria hija", parent_id: raiz.id, node_level: 1, activo: true
      raiz.reload

      hash = Category.get_children raiz.id      

      expect(hash).to eq [
        {optionId: 2, option: "Categoria padre", target: "Categoria padre",hasChild: false, nodeLevel: 2, children:[], permisoId: 2},
        {optionId: 3, option: "Categoria hija", target: "Categoria hija",hasChild: false, nodeLevel: 2, children:[], permisoId: 3}
      ]

    end

  end

  def check_tree_node(node, has_child, node_level, path)
      expect(node.has_child).to eq has_child
      expect(node.node_level).to eq node_level
      expect(node.tree_path).to eq path
  end

end
