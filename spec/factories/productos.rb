FactoryBot.define do
  factory :producto do
    clave "MyString"
    descripcion "MyString"
    costo 1.5
    precio 1.5
    precio_publico 1.5
    impuesto 1.5
    puntos 1.5
    puntos_calificar 1.5
  end
end
