FactoryBot.define do
  factory :cliente_elite_forma_pago do
    cliente_elite nil
    forma_pago "MyString"
    numero_tarjeta "MyString"
    vencimiento "2018-06-14"
    codigo_verificacion "MyString"
    nombre_tarjeta "MyString"
    direccion "MyString"
    ciudad nil
    estado nil
    codigo_postal 1
  end
end
