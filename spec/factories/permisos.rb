FactoryBot.define do
  factory :permiso do
    permiso "MyString"
    descripcion "MyString"
    tipo_accion 1
    parent_id 1
    treepath "MyString"
    node_level 1
    has_child false
    iorder 1
    object "MyString"
  end
end
