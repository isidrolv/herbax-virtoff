FactoryBot.define do
  factory :socio do
    numero "MyString"
    banco nil
    cuenta "MyString"
    tarjeta "MyString"
    apellido_paterno "MyString"
    apellido_materno "MyString"
    nombre "MyString"
    fecha_nacimiento "2018-06-09"
    domicilio_particular "MyString"
    colonia "MyString"
    codigo_postal 1
    documento_de_identificacion "MyString"
    estado_nacimiento_id 1
    sexo "MyString"
    telefono_particular "MyString"
    telefono_celular "MyString"
    email "MyString"
    ciudad nil
    estado nil
    pais_id 1
    rfc "MyString"
    curp "MyString"
    fecha_de_ingreso "2018-06-09"
    nombre_co_titular "MyString"
    fecha_nacimiento_ec "2018-06-09"
    email_ct "MyString"
    telefono_celular_ct "MyString"
    parent_id 1
    tree_path "MyString"
    has_child false
    node_level 1
    copia_ife false
    copia_curp false
    copia_rfc false
  end
end
