FactoryBot.define do
  factory :usuario do
    usuario "MyString"
    password "MyString"
    nombre "MyString"
    email "MyString"
    activo false
    role_id 1
  end
end
