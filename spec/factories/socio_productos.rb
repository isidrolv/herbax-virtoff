FactoryBot.define do
  factory :socio_producto do
    producto nil
    cantidad 1
    clave "MyString"
    puntos 1.5
    precio_publico 1.5
    precio_socio 1.5
    total 1.5
  end
end
