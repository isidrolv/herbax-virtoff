FactoryBot.define do
  factory :socio_forma_pago do
    socio nil
    forma_pago "MyString"
    numero_tarjeta "MyString"
    vencimiento "2018-06-12"
    codigo_verificacion "MyString"
    nombre_tarjeta "MyString"
    direccion "MyString"
    ciudad nil
    estado nil
    codigo_postal 1
  end
end
