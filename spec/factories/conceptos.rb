FactoryBot.define do
  factory :concepto do
    clave "MyString"
    concepto "MyString"
    descripcion "MyText"
    imagen "MyString"
    category nil
    activo false
  end
end
