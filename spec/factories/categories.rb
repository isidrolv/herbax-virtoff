FactoryBot.define do
  factory :category do
    categoria "MyString"
    val1 1
    val2 1
    vals1 "MyString"
    vals2 "MyString"
    parent_id 1
    tree_path "MyString"
    activo false
    has_child false
    node_level 1
    orden 1
    sistema false
    visible false
  end

end
