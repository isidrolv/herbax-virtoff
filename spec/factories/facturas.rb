FactoryBot.define do
  factory :factura do
    folio "MyString"
    fecha "2017-10-17"
    tipo_factura "MyString"
    proveedor nil
    sub_total 1.5
    iva 1.5
    total 1.5
    usuario nil
  end
end
