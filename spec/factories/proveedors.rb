FactoryBot.define do
  factory :proveedor do
    nombre "MyString"
    rfc "MyString"
    domicilio "MyString"
    numero_interior "MyString"
    numero_exterior "MyString"
    ciudad nil
    estado nil
    codigo_postal 1
    telefono1 "MyString"
    telefono2 "MyString"
    telefono3 "MyString"
    contacto "MyString"
  end
end
