FactoryBot.define do
  factory :cliente_elite do
    numero "MyString"
    apellido_paterno "MyString"
    apellido_materno "MyString"
    nombre "MyString"
    fecha_nacimiento "2018-06-14"
    domicilio_particular "MyString"
    colonia "MyString"
    codigo_postal 1
    identificacion "MyString"
    estado_donde_nacio_id 1
    sexo "MyString"
    telefono_particular "MyString"
    telefono_celular "MyString"
    email "MyString"
    ciudad nil
    estado nil
    pais_id 1
    rfc "MyString"
    curp "MyString"
    fecha_de_ingreso "2018-06-14"
    socio nil
  end
end
