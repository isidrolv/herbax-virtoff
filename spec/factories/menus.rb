FactoryBot.define do
  factory :menu do
    option "MyString"
    hint "MyString"
    shortcut "MyString"
    menu_type 1
    url "MyString"
    permiso_id 1
    parent_id 1
    tree_path "MyString"
    has_child false
    node_level 1
    iorder 1
    activo false
  end
end
