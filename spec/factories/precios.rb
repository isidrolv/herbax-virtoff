FactoryBot.define do
  factory :precio do
    fecha "2017-04-06"
    category nil
    concepto nil
    precio_normal 1.5
    precio_pronto_pago 1.5
    fecha_pronto_pago "2017-04-06"
    vigencia_de "2017-04-06"
    vigencia_hasta "2017-04-06"
    activo false
    usuario nil
  end
end
