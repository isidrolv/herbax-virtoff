FactoryBot.define do
  factory :factura_concepto do
    factura nil
    no_identificacion "MyString"
    descripcion "MyString"
    cantidad 1.5
    unidad "MyString"
    valor_unitario 1.5
    importe 1.5
  end
end
