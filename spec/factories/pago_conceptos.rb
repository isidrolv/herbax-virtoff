FactoryBot.define do
  factory :pago_concepto do
    pago nil
    concepto nil
    monto 1.5
    order 1
    check_sum "MyString"
  end
end
