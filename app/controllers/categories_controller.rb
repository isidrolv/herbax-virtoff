class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  # GET /categories
  # GET /categories.json
  def index
    @categories = Category.page(params[:page]).per(10).order(:tree_path)

    respond_to do |format|
      format.html
      format.js 
    end
  end

  # POST /categories/search
  def search
    @categories = Category.where(['categoria like ?', "%#{params[:search_text].upcase}%"]).page(params[:page]).per(10)

    respond_to do |format|
      format.html { render "index"}
      format.js { render "index.js"}
    end
  end

  # GET /categories/1
  # GET /categories/1.json
  def show
  end

  # GET /categories/new
  def new
    @category = Category.new
    @categories_combo = Category.all.order(:tree_path)
  end

  # GET /categories/1/edit
  def edit
    @category = Category.find(params[:id])
    load_combo
  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(category_params)

    respond_to do |format|
      if @category.save
        format.html { redirect_to @category, notice: 'Categoria ha sido creada exitosamente.' }
        format.json { render :show, status: :created, location: @category }
      else
        load_combo
        format.html { render :new }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.json
  def update
    respond_to do |format|
      begin
        if @category.update(category_params)
          format.html { redirect_to @category, notice: 'Categoria ha sido actualizada.' }
          format.json { render :show, status: :ok, location: @category }
        else
          format.html { render :edit }
          format.json { render json: @category.errors, status: :unprocessable_entity }
        end
      rescue Exception => e
        @category.errors[:Error] = ": #{e.message.humanize}"
        format.html { render :edit }      
        format.json { render json: @category.errors, status: :unprocessable_entity }        
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category.destroy
    respond_to do |format|
      format.html { redirect_to categories_url, notice: 'Categoria ha sido eliminada.' }
      format.json { head :no_content }
    end
  end

  def load_combo
    token = @category.tree_path
    @categories_combo = Category.where(['tree_path not like ?', "#{token}%"]).order(:tree_path)
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:categoria, :val1, :val2, :vals1, :vals2, :parent_id, :tree_path, :activo, :has_child, :node_level, :orden, :sistema, :visible)
    end
end
