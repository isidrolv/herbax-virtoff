class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  CONFIG = YAML.load_file("#{Rails.root.to_s}/config/database.yml")

  before_action :authenticate_usuario!

  def check_is_permitted(option)
    if !current_usuario.has_access_to(option)
      redirect_to dashboard_index_path
    end
  end

  def open_db
  	username = CONFIG["development"]["username"]
  	password = CONFIG["development"]["password"]
  	host = CONFIG["development"]["host"]
  	dataserver = CONFIG["development"]["dataserver"]
  	database = CONFIG["development"]["sih_database"]
  	dataserver = CONFIG["development"]["dataserver"]
  	
    TinyTds::Client.new username: username, password: password, host: host, 
    		dataserver: dataserver, database: database, timeout: 10000
  end

end
#"0000199595", "Socio"=>"CASTELLON GARCIA ZILMA ZITALI"
