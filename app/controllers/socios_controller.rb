class SociosController < ApplicationController
  before_action do
    logger.info "#{controller_name}, #{action_name}"
    set_socio if ['show', 'edit', 'update', 'destroy'].include? action_name
    check_is_permitted(4)
  end

  # GET /socios
  # GET /socios.json
  def index
    @socios = Socio.limit(10)

    respond_to do |format|
      format.html
      format.js
      format.json
      format.xml { render xml: @socios }
    end
  end

  # POST /socios/search
  def search
    filter = "%#{params[:search_text].upcase}%"
    @socios = Socio.joins(:usuario)
      .where(['usuarios.nombre like ?', filter]).limit(10)

    respond_to do |format|
      format.html { render "index"}
      format.js { render "index.js"}
      format.json 
    end
  end

  # GET /socios/1
  # GET /socios/1.json
  def show
  end

  # GET /socios/new
  def new
    @socio = Socio.new
    @socio.beneficiarios.build
    @socio.beneficiarios.build
    Producto.all.each do |producto|
      @socio.socio_productos.build producto_id: producto.id, cantidad: 0, clave: producto.clave, puntos: producto.puntos, precio_publico: producto.precio_publico, precio_socio: producto.precio, total: 0
    end
    @socio.socio_forma_pago = SocioFormaPago.new forma_pago: "VISA"
  end

  # GET /socios/1/edit
  def edit
  end

  # POST /socios
  # POST /socios.json
  def create
    @socio = Socio.new(socio_params)

    respond_to do |format|
      if @socio.save
        format.html { redirect_to @socio, notice: 'Socio was successfully created.' }
        format.json { render :show, status: :created, location: @socio }
      else
        format.html { render :new }
        format.json { render json: @socio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /socios/1
  # PATCH/PUT /socios/1.json
  def update
    respond_to do |format|
      if @socio.update(socio_params)
        format.html { redirect_to @socio, notice: 'Socio was successfully updated.' }
        format.json { render :show, status: :ok, location: @socio }
      else
        format.html { render :edit }
        format.json { render json: @socio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /socios/1
  # DELETE /socios/1.json
  def destroy
    @socio.destroy
    respond_to do |format|
      format.html { redirect_to socios_url, notice: 'Socio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_socio
      @socio = Socio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def socio_params
      params.require(:socio).permit(:numero, :banco_id, :cuenta, :tarjeta, :apellido_paterno, :apellido_materno, :nombre, :fecha_nacimiento, :domicilio_particular, :colonia, :codigo_postal, :documento_de_identificacion, :estado_nacimiento_id, :sexo, :telefono_particular, :telefono_celular, :email, :ciudad_id, :estado_id, :pais_id, :rfc, :curp, :fecha_de_ingreso, :nombre_co_titular, :fecha_nacimiento_ec, :email_ct, :telefono_celular_ct, :parent_id, :tree_path, :has_child, :node_level, :copia_ife, :copia_curp, :copia_rfc, beneficiarios_attributes: [:id, :socio_id, :nombre, :porcentaje, :_destroy], socio_productos_attributes: [:id, :socio_id, :producto_id, :cantidad, :clave, :puntos, :precio_publico, :precio_socio, :total, :_destroy], socio_forma_pago_attributes: [:id, :socio_id, :forma_pago, :numero_tarjeta, :vencimiento, :codigo_verificacion, :nombre_tarjeta, :direccion, :ciudad_id, :estado_id, :codigo_postal])
    end
end
