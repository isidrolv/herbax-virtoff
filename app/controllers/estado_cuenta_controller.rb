require ::Rails.root.join('app','pdfs','estado-cuenta-pdf.rb')
require 'prawn'

class EstadoCuentaController < ApplicationController

  before_action { check_is_permitted(2) }

  def index
    if current_usuario.nil?
      respond_to do |wants|
        wants.html {  }
      end
    else
      set_global_vars
      respond_to do |format|
        format.html
        format.pdf do
           data = get_data params
           logger.info data
           periodo = get_periodo params[:periodo]
           render_edo_cta(data, periodo, current_usuario.email) 
        end
      end
    end
  end

  def show
    redirect_to :index
  end

  def create
    begin
      if params[:format] == 'pdf'
          respond_with_report(estado_cuenta_params)
      else
        @cliente = params[:estado_cuentum][:cliente]
        @periodo = get_periodo_id params[:estado_cuentum][:anio], params[:estado_cuentum][:mes], params[:estado_cuentum][:quincena]
        @pais = params[:estado_cuentum][:pais]
        @modalidad = params[:estado_cuentum][:modalidad]
        respond_to do |format|
          format.html { flash[:alert] = "Peticion inválida..." 
            render "index.html" }
          format.js 
        end
      end
    rescue Exception => e
      logger.error e.to_s
      respond_to do |format|
        format.pdf { render_error_doc(estado_cuenta_params[:cliente]) }
      end
    end
  end

  def respond_with_report(params)
    data = get_data params
    p data
    periodo = get_periodo params[:periodo]
    respond_to do |format|
      format.pdf { render_edo_cta(data, periodo, current_usuario.email) }
    end
  end

  def render_edo_cta(data, periodo, usuario_login)
    pdf = EstadoCuentaPDF.new(data, periodo, usuario_login)
    send_data pdf.render, 
        filename: "estado_cuenta.pdf",
        type: 'application/pdf',
        disposition: 'inline'
  end

  def render_error_doc(cliente)
    pdf = Prawn::Document.new do 
      text "Error al generar el reporte, el cliente #{cliente} puede no tener datos."
    end
    send_data pdf.render, 
        filename: "estado_cuenta.pdf",
        type: 'application/pdf',
        disposition: 'inline'
  end

  private
  def set_global_vars
      @cliente = current_usuario.socio.numero
      #@columns = { "Id_Factura"=>"Factura", "DocAnt"=>"Orden", "Estatus"=>"Estatus", "Id_Producto"=>"Codigo", "Producto"=>"Producto", "Cantidad"=>"Cantidad", "Importe"=>"Monto", "Puntos"=>"Puntos", "F_Factura" => "Fecha", "Impuesto" => "Impuesto", "Puntos_Calificar" => "Puntos Calificables", "Total" => "Total", "Almacen"=>"Almacen"}
      @mes = I18n.t("date.abbr_month_names").map {|mes| mes.humanize if mes }
      @paises = [{codigo: 'MX', id: '01'}, {codigo: 'GT', id: '02'}, {codigo: 'USA', id: '03'}]
  end

  def estado_cuenta_params
    params.require(:estado_cuentum).permit([:periodo, :cliente, :almacen, :pais, :anio, :mes, :quincena, :modalidad])
  end

  def get_data(params)
    periodo = params[:periodo]
    cliente = params[:cliente]
    almacen = params[:almacen]
    pais = params[:pais]
    modalidad = params[:modalidad]

    stored_procs = [
        "EXEC CO_sp_se_EdoCta_Enc #{periodo},'#{cliente}', #{almacen}, '#{pais}'",
        "EXEC CO_sp_se_EdoCta_Dtl #{periodo},'#{cliente}', #{almacen}, '#{pais}'",
        "EXEC CO_sp_se_EdoCta_PercDed_Cte #{periodo},'#{cliente}', #{almacen}, '#{pais}'",
        "EXEC CO_sp_se_EdoCta_VolNivel  #{periodo},'#{cliente}', #{almacen}, '#{pais}'",
        "EXEC CO_sp_se_EdoCta_SociosGen #{periodo},'#{cliente}', #{almacen}, '#{pais}'",
        "EXEC CO_sp_se_EdoCta_DtlNvos #{periodo},'#{cliente}', #{almacen}, '#{pais}'"
    ]

    client = open_db

    results = []

    stored_procs.each_with_index do |sproc, index|
      Rails.logger.info "#{index} .- #{sproc}" 
      if ([1,5].include?(index)) && (modalidad == '01')
        result_set = []
      else 
        result_set = client.execute(sproc)
        Rails.logger.info result_set.each {|row| row}
        Rails.logger.info result_set.count
      end
      results[index] = nil
      if result_set.count > 0
        results[index] = result_set.map {|row| row.map {|key, value| [key, value.is_a?(BigDecimal) ? value.to_f : value||'']}.to_h}
      end
      Rails.logger.info "results[#{index}] => #{results[index]}"
    end
    results
  end

  def get_periodo(id_periodo)
    client = open_db
    periodos = client.execute "SELECT * FROM CU_tb_Periodos where id_periodo = #{id_periodo}"
    per = periodos.first
    #p per
    per["Inicio"].strftime("%^B %Y")
  end

  def get_periodo_id(anio, mes, quincena)
    client = open_db
    sql = "SELECT * FROM CU_tb_Periodos where anio = #{anio} and mes = #{mes} and quincena = #{quincena} and Id_Empresa = 1"
    logger.info sql
    periodos = client.execute sql
    per = periodos.first
    #p per
    per["Id_Periodo"]
  end

end
