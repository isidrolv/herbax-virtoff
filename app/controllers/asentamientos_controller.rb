class AsentamientosController < ApplicationController
  before_action :set_asentamiento, only: [:show, :edit, :update, :destroy]

  # GET /asentamientos
  # GET /asentamientos.json
  def index
    @asentamientos = Asentamiento.all
  end

  # GET /asentamientos/1
  # GET /asentamientos/1.json
  def show
    respond_to do |format|
      format.js { }
    end
  end

  # GET /asentamientos/new
  def new
    @asentamiento = Asentamiento.new
  end

  # GET /asentamientos/1/edit
  def edit
  end

  # POST /asentamientos
  # POST /asentamientos.json
  def create
    @asentamiento = Asentamiento.new(asentamiento_params)

    respond_to do |format|
      if @asentamiento.save
        format.html { redirect_to @asentamiento, notice: 'Asentamiento was successfully created.' }
        format.json { render :show, status: :created, location: @asentamiento }
      else
        format.html { render :new }
        format.json { render json: @asentamiento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /asentamientos/1
  # PATCH/PUT /asentamientos/1.json
  def update
    respond_to do |format|
      if @asentamiento.update(asentamiento_params)
        format.html { redirect_to @asentamiento, notice: 'Asentamiento was successfully updated.' }
        format.json { render :show, status: :ok, location: @asentamiento }
      else
        format.html { render :edit }
        format.json { render json: @asentamiento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /asentamientos/1
  # DELETE /asentamientos/1.json
  def destroy
    @asentamiento.destroy
    respond_to do |format|
      format.html { redirect_to asentamientos_url, notice: 'Asentamiento was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_asentamiento
      @asentamiento = Asentamiento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def asentamiento_params
      params.require(:asentamiento).permit(:nombre, :tipo_asentamiento_id, :codigo_postal, :ciudad_id)
    end
end
