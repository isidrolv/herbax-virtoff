class PlanDeNegociosController < ApplicationController
  before_action :set_plan_de_negocio, only: [:show, :edit, :update, :destroy]

  # GET /plan_de_negocios
  # GET /plan_de_negocios.json
  def index
    @plan_de_negocios = PlanDeNegocio.all
  end

  # GET /plan_de_negocios/1
  # GET /plan_de_negocios/1.json
  def show
  end

  # GET /plan_de_negocios/new
  def new
    @plan_de_negocio = PlanDeNegocio.new
  end

  # GET /plan_de_negocios/1/edit
  def edit
  end

  # POST /plan_de_negocios
  # POST /plan_de_negocios.json
  def create
    @plan_de_negocio = PlanDeNegocio.new(plan_de_negocio_params)

    respond_to do |format|
      if @plan_de_negocio.save
        format.html { redirect_to @plan_de_negocio, notice: 'Plan de negocio was successfully created.' }
        format.json { render :show, status: :created, location: @plan_de_negocio }
      else
        format.html { render :new }
        format.json { render json: @plan_de_negocio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /plan_de_negocios/1
  # PATCH/PUT /plan_de_negocios/1.json
  def update
    respond_to do |format|
      if @plan_de_negocio.update(plan_de_negocio_params)
        format.html { redirect_to @plan_de_negocio, notice: 'Plan de negocio was successfully updated.' }
        format.json { render :show, status: :ok, location: @plan_de_negocio }
      else
        format.html { render :edit }
        format.json { render json: @plan_de_negocio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plan_de_negocios/1
  # DELETE /plan_de_negocios/1.json
  def destroy
    @plan_de_negocio.destroy
    respond_to do |format|
      format.html { redirect_to plan_de_negocios_url, notice: 'Plan de negocio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_plan_de_negocio
      @plan_de_negocio = PlanDeNegocio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def plan_de_negocio_params
      params.require(:plan_de_negocio).permit(:fecha, :socio_id, :folio, :monto, :nivel)
    end
end
