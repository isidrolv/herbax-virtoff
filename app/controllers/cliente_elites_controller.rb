class ClienteElitesController < ApplicationController
  before_action do 
    logger.info "#{controller_name}, #{action_name}"
    set_cliente_elite if ['show', 'edit', 'update', 'destroy'].include? action_name
    check_is_permitted(5)
  end

  # GET /cliente_elites
  # GET /cliente_elites.json
  def index
    @cliente_elites = ClienteElite.all.limit(10)

    respond_to do |format|
      format.html
      format.js
      format.pdf
      format.json
      format.xml { render xml: @cliente_elites }
    end
  end

  # POST /cliente_elites/search
  def search
    filter = "%#{params[:search_text].upcase}%"
    @cliente_elites = ClienteElite.where(['nombre like ? or apellido_paterno like ? or apellido_materno like ?', filter, filter, filter]).limit(10)

    respond_to do |format|
      format.html { render "index"}
      format.js { render "index.js"}
    end
  end

  # GET /cliente_elites/1
  # GET /cliente_elites/1.json
  def show
  end

  # GET /cliente_elites/new
  def new
    @cliente_elite = ClienteElite.new
    Producto.all.each do |producto|
      @cliente_elite.cliente_elite_productos.build producto_id: producto.id, cantidad: 0, clave: producto.clave, puntos: producto.puntos, precio_publico: producto.precio_publico, precio_cliente: producto.precio, total: 0
    end
    @cliente_elite.cliente_elite_forma_pago = ClienteEliteFormaPago.new forma_pago: "VISA"
  end

  # GET /cliente_elites/1/edit
  def edit
  end

  # POST /cliente_elites
  # POST /cliente_elites.json
  def create
    @cliente_elite = ClienteElite.new(cliente_elite_params)

    respond_to do |format|
      if @cliente_elite.save
        format.html { redirect_to @cliente_elite, notice: 'Cliente elite was successfully created.' }
        format.json { render :show, status: :created, location: @cliente_elite }
      else
        format.html { render :new }
        format.json { render json: @cliente_elite.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cliente_elites/1
  # PATCH/PUT /cliente_elites/1.json
  def update
    respond_to do |format|
      if @cliente_elite.update(cliente_elite_params)
        format.html { redirect_to @cliente_elite, notice: 'Cliente elite was successfully updated.' }
        format.json { render :show, status: :ok, location: @cliente_elite }
      else
        format.html { render :edit }
        format.json { render json: @cliente_elite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cliente_elites/1
  # DELETE /cliente_elites/1.json
  def destroy
    @cliente_elite.destroy
    respond_to do |format|
      format.html { redirect_to cliente_elites_url, notice: 'Cliente elite was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cliente_elite
      @cliente_elite = ClienteElite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cliente_elite_params
      params.require(:cliente_elite).permit(:numero, :apellido_paterno, :apellido_materno, :nombre, :fecha_nacimiento, :domicilio_particular, :colonia, :codigo_postal, :identificacion, :estado_donde_nacio_id, :sexo, :telefono_particular, :telefono_celular, :email, :ciudad_id, :estado_id, :pais_id, :rfc, :curp, :fecha_de_ingreso, :socio_id, cliente_elite_productos_attributes: [:id, :cliente_elite_id, :producto_id, :cantidad, :clave, :puntos, :precio_publico, :precio_socio, :total, :_destroy], cliente_elite_forma_pago_attributes: [:id, :cliente_elite_id, :forma_pago, :numero_tarjeta, :vencimiento, :codigo_verificacion, :nombre_tarjeta, :direccion, :ciudad_id, :estado_id, :codigo_postal])
    end
end
