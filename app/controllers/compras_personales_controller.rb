require 'currency'
require 'currency/exchange/rate/source/xe'

class ComprasPersonalesController < ApplicationController

  before_action { check_is_permitted(3) }

  def index
    if signed_in?
      begin
        set_global_vars
        @cliente, mes, year = current_usuario.socio.numero, Time.now.month, Time.now.year
        # usuario herbax: 173831, 'diamante16'
        @ventas_mensuales = get_data(@cliente, mes, year)[0]
      rescue Exception => e
        @ventas_mensuales = [{error: e.to_s}]
      end
    end
  end

  def create
    begin
      set_global_vars
      @cliente = compras_personales_params[:cliente]
      mes = compras_personales_params[:mes] 
      year = compras_personales_params[:anio]
      # usuario herbax: 173831, 'diamante16'
      @ventas_mensuales = get_data(@cliente, mes, year)[0]

    rescue Exception => e
      @columns = {error: "Error"}
      @ventas_mensuales = [{error: e.to_s}]
    end      
    respond_to do |format|
      format.js 
    end
  end

private
  def set_global_vars
      @columns = { "Id_Factura"=>"Factura", "DocAnt"=>"Orden", "Estatus"=>"Estatus", "Id_Producto"=>"Codigo", "Producto"=>"Producto", "Cantidad"=>"Cantidad", "Importe"=>"Monto", "Puntos"=>"Puntos", "F_Factura" => "Fecha", "Impuesto" => "Impuesto", "Puntos_Calificar" => "Puntos Calificables", "Total" => "Total", "Almacen"=>"Almacen"}
      @mes = I18n.t("date.abbr_month_names").map {|mes| mes.humanize if mes }    
  end

  def get_data(cliente , mes, year)
    stored_procs = [
      "EXEC VE_sp_se_VentasMensualesPorCliente '#{cliente}', #{mes}, #{year}",
      " SELECT * FROM [VE_tb_ValorPunto] -- EXEC WE_sp_tabs_reporte "
    ]

    client = open_db

    results = []

    stored_procs.each_with_index do |sproc, index|
      p sproc
      result_set = client.execute(sproc)
      if result_set.count == 0
        meses = I18n.t("date.month_names").map { |mes| mes.humanize if mes }
        #p meses
        raise "No hay datos para el socio #{cliente} en el mes de #{meses[mes.to_i]} - #{year}"
      end
      results[index] = result_set.map {|row| row.map {|key, value| [key, value.is_a?(BigDecimal) ? number_to_currency(value.to_f) : value.is_a?(DateTime) ? value.strftime("%D") : value]}.to_h}
      #p "r[i] =>", results[index]
    end
    client.close
    results
  end

  def number_to_currency(value)
    unless value 
      ""
    else
      number = Currency.Money(value, :USD)
      number.to_s.gsub('USD', '')
    end
	end

  def compras_personales_params
    params.require(:compras_personales).permit(:cliente, :mes, :anio)
  end

end
