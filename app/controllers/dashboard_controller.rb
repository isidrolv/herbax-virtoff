class DashboardController < ApplicationController
  
  def index
      @tree_depth = 0
      @chart = []
  	if signed_in? 
		data_table = GoogleVisualr::DataTable.new

		# Add Column Headers
		data_table.new_column('string', 'Nombre' )
		data_table.new_column('string', 'Manager')
		data_table.new_column('string', 'ToolTip')
		

		filter = "%" + current_usuario.socio_id.to_s.rjust(5,'0') + "%"
		@tabla = Socio.where('tree_path like ? and id <> ?', filter, current_usuario.socio_id).order(:tree_path).map do | socio |
			[ socio.nombre_completo.humanize, socio.parent.to_s.humanize || '', '']
		end
		p @tabla
		data_table.add_rows(@tabla)
		options = { width: 800, height: 500, title: 'Red del Socio' }
		@chart = GoogleVisualr::Interactive::OrgChart.new(data_table, options)
	end
  end

end
