require 'prawn'
require 'prawn/table'
require 'currency'
require 'currency/exchange/rate/source/xe'

class EstadoCuentaPDF < Prawn::Document

	def initialize(data, periodo, usuario_login)
		begin			
			@data, @periodo, @usuario_login = data, periodo, usuario_login

			Prawn::Font::AFM.hide_m17n_warning = true
			super(:margin_left => 0, :margin_right => 0, :margin_top => 0, :margin_bottom => 10) do
				numbering_options = { 
					:start_count_at => 1, :at => [bounds.right - 150, 730], 
					:align => :right, :size => 8
				}
				
				font = 'Arial'
				font_size = 8

				#stroke_axis

				# header
				draw_header 
				draw_sub_header

				# seccion 1 - 1a Columna
				distr_info
				draw_info_grado
				draw_volumen_compra
				draw_distr_x_generacion

				# seccion 2 - 2a Columna
				draw_estado_cuenta
				draw_oficina_virtual

				# seccion 3 - info distribuidores
				draw_red_distribuidores
				draw_footer

				number_pages("Pag. <page> de <total>", numbering_options)
			end
		rescue Exception => e
			raise "Error en EstadoCuentaPDF #{e.to_s}"
		end
	end

	def draw_header
		p "drawing header, data = #{@data[0]}"
		if @data[0] == nil
			p "data is null"
			id_periodo = '293'
			periodo_str = "Perìodo de:  (#{id_periodo})"
			nombre_socio = 'no hay Informacion de socio'
		else 
			p "data has value"
			header_data = @data[0].first	
			#p header_data
			id_periodo = header_data["Id_Periodo"]
			periodo_str = "Perìodo de: #@periodo (#{id_periodo})"
			nombre_socio = header_data["Socio"]
		end
		fecha_impresion = "Fecha de Impresión: #{Time.now.to_s}"

		draw_text "ESTADO DE CUENTA", :size=> 10, style: :bold, at: [-10, 710]
		draw_text fecha_impresion, :size=> 7, at: [bounds.right - width_of(fecha_impresion, :size => 7), 713]
		draw_text periodo_str, :size=> 10, at: [bounds.right - width_of(periodo_str, :size => 10), 699]
		draw_text nombre_socio, :size=> 8, style: :bold, at: [bounds.right - width_of(nombre_socio, :size => 8), 682]
	end

	def draw_sub_header
		if @data[0]
			header_data = @data[0].first
			grado = header_data["Grado"]
		else
			grado = "No hay Informacion"
		end
		draw_text "GRADO", :size=> 12, style: :bold, at: [bounds.right - width_of("GRADO", :size => 12), 640]
		draw_text grado, :size=> 12, style: :bold, at: [bounds.right - width_of(grado, :size => 12), 620]
	end

	def distr_info
		if @data[0]
			header_data = @data[0].first
			#p header_data
			id_Cliente = header_data["Id_Cliente"]
			socio = header_data["Socio"]
			direccion = header_data["Direccion"]
			ciudad_estado = "#{header_data["Ciudad"]}, #{header_data["Estado"]}"
			codigo_postal = "C.P.     #{header_data["CP"]}"
			rfc = "RFC.     #{header_data["RFC"]}"
			cda = "CDA.     #{header_data["Almacen"]}"
		else
			id_Cliente = "--"
			socio = "No hay información"
			direccion = "--"
			ciudad_estado = "--"
			codigo_postal = "C.P.     --"
			rfc = "RFC.     --"
			cda = "CDA.     --"
		end
		stroke do
			move_to -10, 590
			line_to 235, 590 
		end

		draw_text "Información del Distribuidor", :size=> 10, style: :bold, at: [-10, 595]
		draw_text id_Cliente, :size=> 8, at: [0, 580]
		draw_text socio, :size=> 8, at: [50, 580]
		draw_text direccion, :size=> 8, at: [0, 570]	
		draw_text ciudad_estado, :size=> 8, at: [0, 550]	
		draw_text codigo_postal, :size=> 8, at: [0, 540]	
		draw_text rfc, :size=> 8, at: [0, 530]	
		draw_text cda, :size=> 8, at: [0, 520]	
	end

	def draw_estado_cuenta
		if @data[2]
			estdo_cuenta_data = @data[2]
			p "Estado de Cuenta info:", estdo_cuenta_data

			# step 1 .. sort data
			sorting_data = {}
			concepto = {}
			estdo_cuenta_data.each do |row|
				unless sorting_data[row["Tipo"]]
					sorting_data[row["Tipo"]] = {}
				end
				unless sorting_data[row["Tipo"]][row["Clave"]]
					sorting_data[row["Tipo"]][row["Clave"]] = {}
				end
				if row["F_Aplicacion"].day < 16
					sorting_data[row["Tipo"]][row["Clave"]]["1a"] = row["Cantidad"].to_f
				else
					sorting_data[row["Tipo"]][row["Clave"]]["2a"] = row["Cantidad"].to_f
				end
				concepto[row["Clave"]] = row["PercepDeduc"]
			end

			# step 2 .. group and sumarize data
			gt_1a_qna = 0; gt_2a_qna = 0; gt_mes = 0;
			rep_data = [['Concepto', '1er. Qna', '2a. Qna', 'Total']]
			sorting_data.each do |key, item|
				rep_data += [[key, '', '', '']]
				suma_1a_qna = 0; suma_2a_qna = 0; suma_mes = 0;
			  	item.each do |key1, quincena|
					total_mes = (quincena["1a"]||0) + (quincena["2a"]||0)
					rep_data += [["#{key1} #{concepto[key1]}", number_to_currency(quincena["1a"]), number_to_currency(quincena["2a"]), number_to_currency(total_mes)]]
					suma_1a_qna += (quincena["1a"]||0)
					suma_2a_qna += (quincena["2a"]||0)
					suma_mes += total_mes
				end
				rep_data += [['SubTotal:', number_to_currency(suma_1a_qna), number_to_currency(suma_2a_qna), number_to_currency(suma_mes)]]
				gt_1a_qna += suma_1a_qna
				gt_2a_qna += suma_2a_qna
				gt_mes += suma_mes
			end
			rep_data += [['Total Neto:', number_to_currency(gt_1a_qna), number_to_currency(gt_2a_qna), number_to_currency(gt_mes)]]
			
			stroke do
				move_to 240, 590
				line_to 540, 590 
			end

			##stroke_axis

			p rep_data
			p concepto
			n_rows = rep_data.count
			draw_text "Estado de Cuenta", :size=> 10, style: :bold, at: [240, 595]
			font_size = 6
			bounding_box([240, 585], width: 300, height: 450) do
				table_options = { :position => :right, 
					:cell_style => {:size => 6, :column_widths=> [99, 67, 67, 67] }, width: 300}
				table(rep_data, table_options) do
				    cells.padding = 1
				    cells.borders = []
				    cells.align = :right
				    0.upto(n_rows-1) {|n| row(n).column(0).align = :left}
				    2.upto(n_rows-3) {|n| 
				    	row(n).column(0).style = {:size=>6}
				    	row(n).column(0).padding = [0,0,0,15]; 
				    }
				    rows(0..1).columns(0..3).font_style = :bold
				    row(n_rows-2).column(0).font_style = :bold
				    row(n_rows-1).columns(0..3).font_style = :bold
				    row(n_rows-1).columns(1..3).borders = [:top]
				end
			end
		end
	end
	
	def draw_info_grado
		if @data[0]
			info_grado = @data[0].first
			p "Informacion de grado:", info_grado
			plan_mercadeo = info_grado["PlanDeMercadeo"]
			puntos_personales = info_grado["Puntos_Personales"]
			directos_calificados = info_grado["Directos_Calificados"]
			valor_punto = info_grado["ValorPunto"]
			compras_personales = info_grado["Compras_Personales"]

			stroke do
				move_to -10, 495
				line_to 235, 495 
			end

			draw_text "Información para determinar el Grado", :size=> 10, style: :bold, at: [-10, 500]
			draw_text "Plan de Mercadeo: ", :size=> 8, style: :bold, at: [0, 485]
			draw_text plan_mercadeo, :size=> 8, at: [120 - width_of(plan_mercadeo, :size => 8), 485]
			fill_color '00AA00'
			draw_text "Puntos personales:", :size=> 8, style: :bold, at: [0, 470]	
			draw_text puntos_personales, :size=> 8, at: [120 - width_of(puntos_personales.to_s, :size => 8), 470]	
			fill_color '000000'
			draw_text "Directos calificados:", :size=> 8, style: :bold, at: [0, 455]	
			draw_text directos_calificados, :size=> 8, at: [120 - width_of(directos_calificados.to_s, :size => 8), 455]	
			draw_text "Valor del punto:", :size=> 8, style: :bold, at: [125, 485]	
			draw_text valor_punto, :size=> 8, at: [230 - width_of(valor_punto.to_s, :size => 8), 485]	
			draw_text "Compra Personal:", :size=> 8, style: :bold, at: [125, 470]	
			draw_text compras_personales, :size=> 8, at: [230 - width_of(compras_personales.to_s, :size => 8), 470]	
		end
	end

	def draw_volumen_compra
		if @data[3]
			volumen_compra_data = @data[3].to_a	
			Rails.logger.info "Volumen de compra: #{volumen_compra_data}" 

			# step 1 .. group and sumarize data
			gt = ['Compra Acumulada:',0,0,0,0,0]
			rep_data = [['Generación', '1', '2', '3', '4', '5']]
			volumen_compra_data.each do |item|
				p item
				renglon = ["Compra:"]
				1.upto(5).each {|n| renglon << item["VC#{n}"].to_f }
				1.upto(5).each do |n| 
					if n == 1
						gt[n] = renglon[n]
					else 
						gt[n] = gt[n-1] + renglon[n]
					end
				end
				rep_data += [renglon]
			end

			rep_data += [
				gt.map do |item|
					item.is_a?(Numeric) ? number_to_currency(item) : item
				end
			]
			
			stroke do
				move_to -10, 430
				line_to 235, 430
			end

			p rep_data
			n_rows = rep_data.count
			sub_title = "Volumen de compra por Generación (Compra Calificable)"
			draw_text sub_title, :size=> 8, style: :bold, at: [-10, 435]
			font_size = 6
			bounding_box([0, 430], width: 250, height: 100) do
				table_options = { :position => :left, 
					:cell_style => {
						:size => 6, :column_widths=> [80, 37, 37, 37, 39] }, 
					width: 230}
				table(rep_data, table_options) do
				    cells.padding = 1
				    cells.borders = []
				    cells.align = :right
				    0.upto(n_rows-1) {|n| row(n).column(0).align = :left; row(n).column(0).font_style = :bold}
				    2.upto(n_rows-3) {|n| 
				    	row(n).column(0).style = {:size=>6}
				    	row(n).column(0).padding = [0,0,0,15];
				    }
				end
			end
		end
	end


	def draw_distr_x_generacion
		if @data[4]
			data = @data[4].to_a[0]
			p "Distribuidores por generacion:", data.count

			# step 1 .. sort data
			# sorting_data = {}
			# concepto = {}

			# step 2 .. group and sumarize data
		  	gs = data.map {|item| item}.delete_if{|name, value| 
		  		!(["G1", "G2", "G3", "G4", "G5", "N1", "N2", "N3", "N4", "N5", "Calificados"].include?(name))
		  	}.map{|k,v| v}
			rep_data = [["Generacion", 1, 2, 3, 4, 5, 'Total']]
			rep_data += [['Calificados'] + gs ]
			
			stroke do
				move_to -10, 370
				line_to 235, 370
			end

			p rep_data
			n_rows = rep_data.count
			draw_text "Distribuidores calificados por generación", :size=> 8, style: :bold, at: [-10, 375]
			font_size = 6
			bounding_box([0, 370], width: 250, height: 150) do
				table_options = { 
					:position => :left, 
					:cell_style => { :size => 6, :column_widths=> [80, 25, 25, 25, 25, 25, 25] }, 
					:width => 230 }
				table(rep_data, table_options) do
				    cells.padding = 1
				    cells.borders = []
				    cells.align = :right
				    0.upto(n_rows-1) {|n| 
				    	row(n).column(0).align = :left
				    	row(n).column(0).font_style = :bold 
				    }
				end
			end
		end
	end


	def draw_oficina_virtual
		if @data[0]
			oficina_virtual_data = @data[0].first.to_h	
			p "Oficina Virtual data:", oficina_virtual_data
			usuario = oficina_virtual_data["Id_Cliente"]

			stroke do
				move_to 240, 370
				line_to 540, 370
			end		

			draw_text "Oficina Virtual", :size=> 8, style: :bold, at: [240, 375]
			font_size = 6
			bounding_box([240, 360], :width => 300, :height => 30) do
				text "Recuerde que puede consultar esta información en su " +
					"Oficina Virtual en la pagina: http://www.herbax.mx", size: 8, style: :bold
			end
			draw_text "Usuario: #{@usuario_login}", :size=> 8, style: :bold, at: [250, 330]	
			draw_text "Contraseña: La que usted definió", :size=> 8, style: :bold, at: [250, 320]	
		end
	end

	def draw_red_distribuidores
		if @data[1]
			data = @data[1].to_a	
			p "Red descendiente distribuidores data:", data.count

			# step 1 .. sort data
			sorting_data = {}
			data.each do |row|
				unless sorting_data[row["Linea"]]
					sorting_data[row["Linea"]] = []
				end
				puts "Loading #{row["Linea"]} ... "
				sorting_data[row["Linea"]] += [row]
			end

			# step 2 draw title
			draw_text "Red descendiente de distribuidores", :size=> 10, style: :bold, at: [-10, 305]
			stroke do
				move_to -10, 300
				line_to 540, 300 
			end
			font_size = 5
			move_down 30

			campos = ["Nivel_Real", "Nivel_Comp", "Generacion", "Id_Cliente_Red", "Cliente", "Grado", 
								"Compra_Volumen", "Puntos_Calificar", "Compras_Comisionables", "Porc_2aGan", 
								"Gan_2", "Porc_3aGan", "Gan_3"]
			sorting_data.each do |key, item|
				#p key, item
				results = [
					['Linea', key, '', '', '', '', '', '', '', '', '', '', ''],
					['Nivel Real', 'Nivel Comp.', 'Gen.', 'No.', 'Nombre del DIH', 'Grado', 'Compra Total', 
							'Puntos Perso.', 'Compras Comis.', '% 2a. Gan', '$ 2a. Gan', '% 3a. Gan', '$ 3a. Gan']
				]
				
				item.each do |row|
					results += [ 
						campos.map {|campo| 
							row[campo].is_a?(BigDecimal) ? number_to_currency(row[campo].to_f) : row[campo] 
						}
					]
				end
				p results.count
				draw_line_group(results)
			end		
			if cursor < 25.0
				start_new_page
			end
		end
	end

	def table_options
		col_widths = [15, 15, 15, 40, 100, 80, 35, 40, 40, 40, 40, 40, 40]
		{ 
			:position => :left, 
			:width => col_widths.inject(:+), 
			:cell_style => { 
					:size => 6, 
					:column_widths => col_widths
			} 
		}
	end

	def draw_line_group(results)
		#stroke_axis
		n_rows = results.count
		table(results, table_options) do
		    cells.padding = 1
		    cells.borders = []
		    #cells.align = :right
		    cells.style = {:size=>6}
		    0.upto(n_rows-1) {|n| 13.times {|m| row(n).column(m).align = (m < 6) ? :left : :right }}
		    rows(0..1).columns(0..12).font_style = :bold
		end
		move_down 20
	end

	def draw_footer 
		#stroke_axis
		bounding_box([-10, 25], :width => 580, :height => 30) do
			p1 = <<-END	
			Los Reembolsos ganados en el mes ya no son acumulables, solo se canjean al siguiente mes por el producto marcado en la tabla que se encuentra en su CDA
			Recuerda que al cambiar tu estatus a baja o inactivo al activarte pierdes tu red
			END
			text "Mensajes del Director:", style: :bold, size: 8
			text p1, size: 8
		end
	end


	def number_to_currency(value)
		unless value 
			""
		else
			number = Currency.Money(value, :USD)
			number.to_s.gsub('USD', '')
		end
	end
	
end