json.array!(@ciudads) do |ciudad|
  json.extract! ciudad, :id, :nombre, :nombre_corto, :estado_id
  json.url ciudad_url(ciudad, format: :json)
end
