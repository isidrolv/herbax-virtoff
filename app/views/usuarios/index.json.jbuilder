json.array!(@usuarios) do |usuario|
  json.extract! usuario, :id, :usuario, :password, :nombre, :email, :activo, :role_id
  json.url usuario_url(usuario, format: :json)
end
