json.extract! precio, :id, :fecha, :category_id, :concepto_id, :precio_normal, :precio_pronto_pago, :fecha_pronto_pago, :vigencia_de, :vigencia_hasta, :activo, :usuario_id, :created_at, :updated_at
json.url precio_url(precio, format: :json)
