require 'tiny_tds'
require 'prawn'
require 'prawn/table'

def draw_header
	header_data = $result[0].first.map {|key, value| [key, value.is_a?(BigDecimal) ? value.to_f : value]}.to_h	
	p header_data
	id_periodo = header_data["Id_Periodo"]
	periodo = get_periodo	id_periodo
	periodo_str = "Perìodo de: #{periodo}(#{id_periodo})"
	fecha_impresion = "Fecha de Impresión: #{Time.now.to_s}"

	draw_text "ESTADO DE CUENTA", :size=> 10, style: :bold, at: [-10, 710]
	draw_text fecha_impresion, :size=> 7, at: [bounds.right - width_of(fecha_impresion, :size => 7), 713]
	draw_text periodo_str, :size=> 10, at: [bounds.right - width_of(periodo_str, :size => 10), 699]
	draw_text header_data["Socio"], :size=> 8, style: :bold, at: [bounds.right - width_of(header_data["Socio"], :size => 8), 682]
end

def draw_sub_header
	header_data = $result[0].first.map {|key, value| [key, value.is_a?(BigDecimal) ? value.to_f : value]}.to_h
	draw_text "GRADO", :size=> 12, style: :bold, at: [bounds.right - width_of("GRADO", :size => 12), 640]
	draw_text header_data["Grado"], :size=> 12, style: :bold, at: [bounds.right - width_of(header_data["Grado"], :size => 12), 620]
end

def distr_info
	header_data = $result[0].first.map {|key, value| [key, value.is_a?(BigDecimal) ? value.to_f : value]}.to_h	
	p header_data
	id_Cliente = header_data["Id_Cliente"]
	socio = header_data["Socio"]
	direccion = header_data["Direccion"]
	ciudad_estado = "#{header_data["Ciudad"]}, #{header_data["Estado"]}"
	codigo_postal = "C.P.     #{header_data["CP"]}"
	rfc = "RFC.     #{header_data["RFC"]}"
	cda = "CDA.     #{header_data["Almacen"]}"

	stroke do
		move_to -10, 590
		line_to 230, 590 
	end

	draw_text "Información del Distribuidor", :size=> 10, style: :bold, at: [-10, 595]
	draw_text id_Cliente, :size=> 8, at: [0, 580]
	draw_text socio, :size=> 8, at: [50, 580]
	draw_text direccion, :size=> 8, at: [0, 570]	
	draw_text ciudad_estado, :size=> 8, at: [0, 550]	
	draw_text codigo_postal, :size=> 8, at: [0, 540]	
	draw_text rfc, :size=> 8, at: [0, 530]	
	draw_text cda, :size=> 8, at: [0, 520]	
end

def draw_estado_cuenta
	edo_cta_data = $result[2].to_a	
	p edo_cta_data

	# step 1 .. sort data
	sorting_data = {}
	edo_cta_data.each do |row|
		unless sorting_data[row["Tipo"]]
			sorting_data[row["Tipo"]] = {}
		end
		unless sorting_data[row["Tipo"]][row["Clave"]]
			sorting_data[row["Tipo"]][row["Clave"]] = {}
		end
		if row["F_Aplicacion"].day < 16
			sorting_data[row["Tipo"]][row["Clave"]]["1a"] = row["Cantidad"].to_f
		else
			sorting_data[row["Tipo"]][row["Clave"]]["2a"] = row["Cantidad"].to_f
		end
	end

	# step 2 .. group and sumarize data
	gt_1a_qna = 0; gt_2a_qna = 0; gt_mes = 0;
	results = [['Concepto', '1er. Qna', '2a. Qna', 'Total']]
	sorting_data.each do |key, item|
		results += [[key, '', '', '']]
		suma_1a_qna = 0; suma_2a_qna = 0; suma_mes = 0;
	  item.each do |key1, quincena|
	  	total_mes = (quincena["1a"]||0) + (quincena["2a"]||0)
	  	results += [[key1, number_to_currency(quincena["1a"], precision: 2), number_to_currency(quincena["2a"], precision: 2), number_to_currency(total_mes, precision: 2)]]
	  	suma_1a_qna += (quincena["1a"]||0)
	  	suma_2a_qna += (quincena["2a"]||0)
	  	suma_mes += total_mes
		end
		results += [['SubTotal:', number_to_currency(suma_1a_qna, precision: 2), number_to_currency(suma_2a_qna, precision: 2), number_to_currency(suma_mes, precision: 2)]]
		gt_1a_qna += suma_1a_qna
		gt_2a_qna += suma_2a_qna
		gt_mes += suma_mes
	end
	results += [['Total Neto:', number_to_currency(gt_1a_qna, precision: 2), number_to_currency(gt_2a_qna, precision: 2), number_to_currency(gt_mes, precision: 2)]]
	
	stroke do
		move_to 250, 590
		line_to 540, 590 
	end

	p results
	draw_text "Estado de Cuenta", :size=> 10, style: :bold, at: [250, 595]
	font_size = 10
	move_down 140
	table(results, :position => :right) do
		width = 290
    cells.padding = 0
    cells.borders = []
    rows.font_style = :bold
    rows.font_size = 8
	end
	# draw_text id_Cliente, :size=> 8, at: [0, 580]
	# draw_text socio, :size=> 8, at: [50, 580]
	# draw_text direccion, :size=> 8, at: [0, 570]	
	# draw_text ciudad_estado, :size=> 8, at: [0, 550]	
	# draw_text codigo_postal, :size=> 8, at: [0, 540]	
	# draw_text rfc, :size=> 8, at: [0, 530]	
	# draw_text cda, :size=> 8, at: [0, 520]	
end

def get_periodo(id_periodo)
	client = open_db
	periodos = client.execute "SELECT * FROM CU_tb_Periodos where id_periodo = #{id_periodo}"
	per = periodos.first
	p per
	per["Inicio"].strftime("%^B %Y")
end

def main
	periodo = 293
	cliente = '0000173831'
	almacen = -1

	sprocs = ["EXEC CO_sp_se_Enc_EstadoDeCuenta #{periodo},'#{cliente}', #{almacen}",
	"EXEC CO_sp_se_Dtl_EstadoDeCuenta #{periodo},'#{cliente}', #{almacen}",
	"EXEC CO_sp_se_CalculoPercDed_PorCliente #{periodo},'#{cliente}', #{almacen}",
	"EXEC CO_sp_se_VolumenCompraPorNivel #{periodo},'#{cliente}', #{almacen}",
	"EXEC CO_sp_se_SociosPorGeneracion #{periodo},'#{cliente}', #{almacen}",
	"EXEC CO_sp_se_Dtl_EstadoDeCuentaNuevos #{periodo},'#{cliente}', #{almacen}"]

	client = open_db

	$result = []

	sprocs.each_with_index do | sproc, index |
		p sproc
		$result[index] = client.execute(sproc)
		$result[index].count
	end


	Prawn::Font::AFM.hide_m17n_warning = true
	Prawn::Document.generate(:margin_left => 0, :margin_right => 0, 
		:margin_top => 0, :margin_bottom => 10) do
		numbering_options = { 
			:start_count_at => 1, :at => [bounds.right - 150, 730], 
			:align => :right, :size => 8
		}
		
		font = 'Arial'
		font_size = 8

		stroke_axis

		draw_header 
		draw_sub_header
		distr_info
		draw_estado_cuenta

		number_pages("Pag. <page> de <total>", numbering_options)

		start_new_page
		draw_header 
		number_pages("Pag. <page> de <total>", numbering_options)

	end
end

main
