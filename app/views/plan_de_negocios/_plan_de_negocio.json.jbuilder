json.extract! plan_de_negocio, :id, :fecha, :socio_id, :folio, :monto, :nivel, :created_at, :updated_at
json.url plan_de_negocio_url(plan_de_negocio, format: :json)
