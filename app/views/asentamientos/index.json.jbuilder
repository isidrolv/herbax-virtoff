json.array!(@asentamientos) do |asentamiento|
  json.extract! asentamiento, :id, :nombre, :tipo_asentamiento_id, :codigo_postal, :ciudad_id
  json.url asentamiento_url(asentamiento, format: :json)
end
