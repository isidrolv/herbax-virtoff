json.extract! proveedor, :id, :nombre, :rfc, :domicilio, :numero_interior, :numero_exterior, :ciudad_id, :estado_id, :codigo_postal, :telefono1, :telefono2, :telefono3, :contacto, :created_at, :updated_at
json.url proveedor_url(proveedor, format: :json)
