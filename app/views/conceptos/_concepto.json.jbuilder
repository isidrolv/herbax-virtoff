json.extract! concepto, :id, :clave, :concepto, :descripcion, :imagen, :category_id, :activo, :created_at, :updated_at
json.url concepto_url(concepto, format: :json)
