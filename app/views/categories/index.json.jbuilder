json.array!(@categories) do |category|
  json.extract! category, :id, :categoria, :val1, :val2, :vals1, :vals2, :parent_id, :tree_path, :activo, :has_child, :node_level, :orden, :sistema, :visible
  json.url category_url(category, format: :json)
end
