json.array!(@roles) do |role|
  json.extract! role, :id, :role_name, :tipo_empresa_id
  json.url role_url(role, format: :json)
end
