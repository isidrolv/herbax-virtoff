json.extract! pago, :id, :folio, :fecha, :monto, :categoria_id, :usuario_id, :alumno_id, :created_at, :updated_at
json.url pago_url(pago, format: :json)
