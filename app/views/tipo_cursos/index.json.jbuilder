json.array!(@tipo_cursos) do |tipo_curso|
  json.extract! tipo_curso, :id, :cve_curso, :descripcion, :estatus, :total_curso, :a002, :a003, :b001, :b002, :b003, :b004, :c003, :p330
  json.url tipo_curso_url(tipo_curso, format: :json)
end
