json.extract! factura, :id, :folio, :fecha, :tipo_factura, :proveedor_id, :sub_total, :iva, :total, :usuario_id, :created_at, :updated_at
json.url factura_url(factura, format: :json)
