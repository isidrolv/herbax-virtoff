json.array!(@permisos) do |permiso|
  json.extract! permiso, :id, :permiso, :descripcion, :tipo_accion, :parent_id, :treepath, :node_level, :has_child, :iorder, :object
  json.url permiso_url(permiso, format: :json)
end
