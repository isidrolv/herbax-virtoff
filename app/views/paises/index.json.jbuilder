json.array!(@paises) do |paise|
  json.extract! paise, :id, :pais
  json.url paise_url(paise, format: :json)
end
