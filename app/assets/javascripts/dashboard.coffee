# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->

	$('#ajax-select').selectpicker({
        liveSearch: true,
        style: 'btn',
        title: 'Buscar'
    }).ajaxSelectPicker({
        ajax: {
            url: '/paises/search',
            type: 'POST',
            dataType: 'json',	
            data: (something)-> 
                params = {
                    search_text: '{{{q}}}'
                };
                return params
        },
        locale: {
            emptyTitle: 'Buscar pa&iacute;s...'
        },
        preprocessData: (data) -> 
            results = []
            for item in data
                results.push({
                    'value': item.id,
                    'text': item.pais,
                    'disabled': false})
            return results
        ,
        preserveSelected: false })
 