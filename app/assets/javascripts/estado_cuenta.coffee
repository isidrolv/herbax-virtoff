# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ -> 

	getQuincena = -> 
		options = $("input[name=quincena]")
		result = null
		for item in options
			if item.checked == true
				result = item.value
		return result

	getPais = -> 
		options = $("input[name=pais]")
		result = null
		for item in options
			if item.checked == true
			   result = item.value
		return result

	$(".btn-control2").click -> 
		$(".btn-control2").removeClass('active')
		$.post "/estado_cuenta/",
  			estado_cuentum:
  				cliente: $(this).attr("data-cid") 
  				anio: $(this).attr("data-year")
  				mes: $(this).attr("data-month")
  				modalidad: $('#modalidad').val()
  				quincena: getQuincena()
  				pais: getPais()
  			format: "js"
		$(this).addClass("active")
		event.preventDefault()