# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ -> 
  $('#search-title-clienteelite').text 'Clientes Elite'
  $('#txtSearchClienteElite').attr "placeholder", "buscar Cliente Elite..."

$(document).ready ->
	$('#txtSearchClienteElite').keyup ->
  		$.post '/cliente_elites/search',
  			search_text: $('#txtSearchClienteElite').val()
  			format: "js"

	$('#cliente_elite_socio_id').selectpicker({
        liveSearch: true,
        style: 'btn-secondary',
        title: 'Buscar patrocinador'
    }).ajaxSelectPicker({
        ajax: {
            url: '/socios/search',
            type: 'POST',
            dataType: 'json',	
            data: (something)-> 
                params = {
                    search_text: '{{{q}}}'
                };
                return params
        },
        locale: {
            emptyTitle: 'Buscar socios...'
        },
        preprocessData: (data) -> 
            results = []
            for item in data
                results.push({
                    'value': item.id,
                    'text': [item.numero,' - ',item.nombre_completo].join(""),
                    'disabled': false})
            return results
        ,
        preserveSelected: true })
