# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
	$('form').on 'click', '.borrar-beneficiario', (event) -> 
		$(this).prev('input[type=hidden]').val('1')
		$(this).closest('tr').hide()
		event.preventDefault()
		
	$('form').on 'click', '.add_fields', (event) -> 
		time = new Date().getTime()
		regexp = new RegExp($(this).data('id'), 'g')
		$('#beneficiarios_grid').append($(this).data('fields').replace(regexp, time))
		event.preventDefault()

$(document).ready ->
	$('#txtSearchSocio').keyup ->
  		$.post '/socios/search',
  			search_text: $('#txtSearchSocio').val()
  			format: "js"
 

	$('#socio_parent_id').selectpicker({
        liveSearch: true,
        style: 'btn-secondary',
        title: 'Buscar patrocinador'
    }).ajaxSelectPicker({
        ajax: {
            url: '/socios/search',
            type: 'POST',
            dataType: 'json',	
            data: (something)-> 
                params = {
                    search_text: '{{{q}}}'
                };
                return params
        },
        locale: {
            emptyTitle: 'Buscar socios...'
        },
        preprocessData: (data) -> 
            results = []
            for item in data
                results.push({
                    'value': item.id,
                    'text': [item.numero,' - ',item.nombre_completo].join(""),
                    'disabled': false})
            return results
        ,
        preserveSelected: true })

	$('#socio_parent_id').on 'changed.bs.select', (e, clickedIndex, isSelected, previousValue) ->
		try
			tokens = $(e.target)[0]
				.options[clickedIndex].label.split(" - ")
			numero = tokens[0]
			nombre = tokens[1]
			$('#nombre_patrocinador').val(nombre)
			$('#numero_socio_patrocinador').val(numero) 
		catch error
			console.log error
