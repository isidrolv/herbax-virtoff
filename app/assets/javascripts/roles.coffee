# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
	try  
		actionName = $("form[class*='_role']").first().attr("action"); 
		if (actionName.match("/roles"))
			$("label[for$='role_name']").text "Nombre del Rol"
			$("label[for$='can_see_index']").text "Ver lista"
			$("label[for$='can_show']").text "Ver elemento"
			$("label[for$='can_create_new']").text "Crear nuevo"
			$("label[for$='can_edit']").text "Editar"
			$("label[for$='can_delete']").text "Borrar"
			$("label[for$='can_print']").text "Imprimir"
			$("label[for$='can_export']").text "Exportar"

			$('.panel-body .row').mouseover(
				-> $(this).css({'background-color': 'honeydew'})
				event.preventDefault()
			)
			$('.panel-body .row').mouseout(
				-> $(this).css({'background-color': ''})
				event.preventDefault()
			)
	catch e then ->
		{ 
			# ignore 
		}
	finally 
	   # do nothing