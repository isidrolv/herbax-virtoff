class Permiso < ActiveRecord::Base
	belongs_to :role, :class_name => "Role", :foreign_key => "role_id"

	def mensaje
		{0 => {true => "Ver listado", false => ""}, 1 => {true => "Ver elemento", false => ""},
		2 => {true => "Crear nuevo", false => ""}, 3 => {true => "Editar", false => ""},
		4 => {true => "Eliminar", false => ""}, 5 => {true => "Imprimir", false => ""},
		6 => {true => "Exportar", false => ""}}
	end

	def can_do
		str = ""
		str += "<li>#{mensaje[0][can_see_index||false] || ''}</li>" if can_see_index
		str += "<li>#{mensaje[1][can_show||false] || ''}</li>" if can_show
		str += "<li>#{mensaje[2][can_create_new||false] || ''}</li>" if can_create_new
		str += "<li>#{mensaje[3][can_edit||false] || ''}</li>" if can_edit
		str += "<li>#{mensaje[4][can_delete||false] || ''}</li>" if can_delete
		str += "<li>#{mensaje[5][can_print||false] || ''}</li>" if can_print
		str += "<li>#{mensaje[6][can_export||false] || ''}</li>" if can_export
		str
	end

	def to_s
		"<span>En #{descripcion} puede: <ul>#{can_do}</ul></span><br>"
	end
end
