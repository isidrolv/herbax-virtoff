class Role < ActiveRecord::Base
	has_many :permisos, :class_name => "Permiso", 
		:foreign_key => "role_id", :dependent => :destroy

	has_many :usuarios, :class_name => "Usuario", :foreign_key => "role_id"

	accepts_nested_attributes_for :permisos, 
    	reject_if: proc { |attributes| attributes['descripcion'].blank? }, 
    	allow_destroy: true

    validates_presence_of :role_name, :on => :create, :message => "es un dato obligatorio"

    def to_s
    	role_name
    end

    def permisos_s
    	self.permisos.map {|permiso| permiso.to_s }.join("").html_safe
    end

end
