class PlanDeNegocio < ActiveRecord::Base
  belongs_to :socio

  validates_presence_of :socio_id, :on => :create, :message => "can't be blank"
end
