class Banco < ActiveRecord::Base
	validates_presence_of :nombre, :on => :create, :message => "can't be blank"


	def to_s
		nombre
	end
end
