class Pago < ActiveRecord::Base
  belongs_to :usuario
  belongs_to :alumno
  belongs_to :categoria, :class_name => "Category", :foreign_key => "categoria_id"

  validates_presence_of :categoria_id, :message => 'Por favor seleccione una categoria'
  validates_presence_of :monto, :on => :create, :message => 'debe ser mayor que 0'
  validates_presence_of :alumno_id, :message => 'no debe ser blanco'
  validates_presence_of :concepto, :message => 'no puede estar en blanco'
end
