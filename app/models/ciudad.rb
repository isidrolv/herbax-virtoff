class Ciudad < ActiveRecord::Base
	belongs_to :estado, class_name: 'Estado', foreign_key: 'estado_id'

	validates_presence_of :nombre, on: :create, message: 'no puede estar en blanco'
	validates_presence_of :estado_id, on: :create, message: 'debe seleccionar un estado'

	def to_s
		nombre
	end
end
