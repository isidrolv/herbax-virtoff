class Menu < ActiveRecord::Base
	has_many :children, :class_name => "Menu", :foreign_key => "parent_id"
	belongs_to :parent, :class_name => "Menu", :foreign_key => "parent_id"

	validates_presence_of :option, :on => :create, :message => "no puede ser en blanco"
	before_update :do_before_update
	after_create :do_after_create

	def do_before_update
		#p "executing menu #{self.id} before_save"
		if self.id == self.parent_id
			raise "No pueden ser iguales id y el parent_id"
		else
			if has_children
				raise 'se esta usando un parent_id y ya es hijo'
			else
				update_tree
			end
		end
	end

	def do_after_create
		#p "executing menu after_create"
		self.save
	end

	def update_tree
		node = self
		_tree_path = ""
		_node_level = 0
		parent_found = true
		while parent_found do 
			_node_level += 1
			_tree_path = node.id.to_s.rjust(5,'0') + "." + _tree_path
			#p _tree_path
			if node.parent_id
				#p "parent to search #{node.parent_id}"
				node = Menu.find(node.parent_id)
			else
				parent_found = false
			end
		end
		Menu.transaction do
			self.tree_path = _tree_path[0..-2]
			self.node_level = _node_level
			cuantos = Menu.where(parent_id: self.id).count
			#p "cuantos #{cuantos}"
			self.has_child = cuantos > 0
			#p "udpate menu ended"
		end
		begin
			if !self.parent_id.nil?
  			self.parent.save # update tree
  		end
  	rescue => e
  		p e
  	end
  end

  def menu_tree
  	"-" * self.node_level + ">" + self.option
  end

  def ids
  	self.id.to_s.rjust(3,'0')
  end

  def self.get_children(parent_id, permitted_ids)
  	begin
  		#p "getting child nodes for #{parent_id}"
  		p permitted_ids
  		options = Menu.where(tipo_permiso_id: permitted_ids, activo: true, parent_id: parent_id).order(:tree_path).map do |item|
  			{
  				optionId: item.id, option: item.option, target: item.url, 
  				hasChild: item.has_child, nodeLevel: item.node_level, 
  				children: Menu.get_children(item.id, permitted_ids), permisoId: item.tipo_permiso_id
  			}
  		end
  	rescue Exception => ex
  		p ex.to_s
  	end
  	return options
  end

  def self.as_array(usuario)
    self.get_children nil, usuario.role.permisos.map{|mp| mp.tipo_permiso_id if mp.can_see_index }
  end

  def has_children
  	#p "children for #{self.id}"
  	ids = children.map {|m| m.id} 
  	#p ids
  	#p ids.include? self.parent_id
  	ids.include? self.parent_id
  end

end