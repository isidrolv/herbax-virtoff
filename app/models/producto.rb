class Producto < ActiveRecord::Base
	has_many :socio_productos, :class_name => "SocioProducto", :foreign_key => "producto_id"

	def to_s
		descripcion
	end
end
