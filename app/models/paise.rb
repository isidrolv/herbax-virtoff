class Paise < ActiveRecord::Base
	has_many :estados, class_name: 'Estado', foreign_key: 'pais_id'
	has_many :socios, :class_name => "Socio", :foreign_key => "pais_id"
	has_many :cliente_elites, :class_name => "ClienteElite", :foreign_key => "pais_id"

	validates_presence_of :pais, on: :create, message: 'No puede estar en blanco'

	def to_s
	  pais
	end
end
