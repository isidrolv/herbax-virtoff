class ClienteElite < ActiveRecord::Base
  belongs_to :ciudad
  belongs_to :estado
  belongs_to :socio
  belongs_to :pais, :class_name => "Paise", :foreign_key => "pais_id"
  has_many :cliente_elite_productos, :class_name => "ClienteEliteProducto", 
      :foreign_key => "cliente_elite_id", :dependent => :destroy
  has_one :cliente_elite_forma_pago, :class_name => "ClienteEliteFormaPago", 
      :foreign_key => "cliente_elite_id", :dependent => :destroy

  accepts_nested_attributes_for :cliente_elite_productos, 
    reject_if: proc { |attributes| attributes['cantidad'].blank? }, 
    allow_destroy: true

  accepts_nested_attributes_for :cliente_elite_forma_pago, 
    reject_if: proc { |attributes| attributes['forma_pago'].blank? }, 
    allow_destroy: true

  def nombre_completo
  	"#{nombre} #{apellido_paterno} #{apellido_materno}"
  end

  def to_s
    nombre_completo
  end
end
