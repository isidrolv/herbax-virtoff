class Estado < ActiveRecord::Base
  has_many :ciudads, class_name: 'Ciudad', foreign_key: 'estado_id'
  belongs_to :pais, class_name: 'Paise', foreign_key: 'pais_id'

  validates_presence_of :estado, on: :create, message: 'no puede ser blanco'
  validates_presence_of :clave, on: :create, message: 'no puede ser blanco'
  validates_presence_of :pais_id, on: :create, message: 'es obligatorio'


  def to_s
  	clave
  end
end
