class TreeHelper

	def self.calculate_tree(entity, node)
    instance = entity.constantize
    tree_path = ''
    node_level = 0
    parent_found = true
    while parent_found do
      node_level += 1
      tree_path = node.id.to_s.rjust(5, '0') + '.' + tree_path
      if node.parent_id
        node = instance.find(node.parent_id)
        parent_found = node.id ? true : false
      else
        parent_found = false
      end
    end
    cuantos = instance.where(parent_id: node.id).count
    yield [tree_path[0..-2], node_level, cuantos]
	end

end