class Factura < ActiveRecord::Base
	has_many :factura_conceptos, :class_name => "FacturaConcepto", :foreign_key => "reference_id"
  belongs_to :proveedor
  belongs_to :usuario

	accepts_nested_attributes_for :factura_conceptos, 
		reject_if: proc { |attributes| attributes['no_identificacion'].blank? }, 
		allow_destroy: true

end
