class Category < ActiveRecord::Base
  has_many :children, class_name: 'Category', foreign_key: 'parent_id'
  belongs_to :parent, class_name: 'Category', foreign_key: 'parent_id'
  has_many :documentos, class_name: 'AlumnoDocumento', foreign_key: 'documento_id'
  has_many :servicios, :class_name => "AlumnoServicio", :foreign_key => "servicio_id"

  validates_presence_of :categoria, message: 'no puede estar en blanco'

  before_update :do_before_update
  after_create :do_after_create

  def do_before_update
    p "executing category #{self.id} before_save"
    if self.id == self.parent_id
      raise 'No pueden ser iguales id y el parent_id'
    else
      if has_children
        raise 'se esta usando un parent_id y ya es hijo'
      else
        update_tree
      end
    end
  end

  def do_after_create
    p 'executing category after_create'
    self.save
  end

  def update_tree
    node = self
    _tree_path = ''
    _node_level = 0
    parent_found = true
    while parent_found do 
      _node_level += 1
      _tree_path = node.id.to_s.rjust(5, '0') + '.' + _tree_path
      #p "#{self.id}=> tree_path: #{_tree_path}, parent_id: #{node.parent_id}"
      if node.parent_id
        #p "parent to search #{node.parent_id}"
          node = Category.find(node.parent_id)
          parent_found = node.id ? true : false
      else
        parent_found = false
      end
    end
    Category.transaction do
      self.tree_path = _tree_path[0..-2]
      self.node_level = _node_level
      #p "checkin for 'parent_id = #{self.id}'"
      cuantos = Category.where(parent_id: self.id).count
      #p "cuantos #{cuantos}"
      self.has_child = cuantos > 0
      #p "udpate category ended"
    end
    begin
      if !self.parent_id.nil?
        self.parent.save # update tree
      end
    rescue => e
      p e
    end
    #p self
  end

  def category_tree
    '---' * node_level + '>' + categoria
  end

  def ids
    id.to_s.rjust(3, '0')
  end

  def self.get_children(parent_id)
    p "getting child nodes for #{parent_id}"
    @child_nodes = Category.where(activo: true, parent_id: parent_id).order(:tree_path)
    options = @child_nodes.map do |item|        
      { optionId: item.id, option: item.categoria, target: item.categoria, hasChild: item.has_child, 
        nodeLevel: item.node_level, children: Category.get_children(item.id),
        permisoId: item.id}
    end
    return options
  end

  def has_children
    p "children for #{self.id}"
      ids = children.map {|m| m.id} 
      p ids
      p ids.include? self.parent_id
      ids.include? self.parent_id
  end

  def to_s
    categoria
  end
end
