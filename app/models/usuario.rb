class Usuario < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, 
  		:rememberable, :trackable, :validatable
  		
  belongs_to :role, :class_name => "Role", :foreign_key => "role_id"
  belongs_to :socio, :class_name => "Socio", :foreign_key => "socio_id"

  def has_access_to(items)
  	if role.permisos
  		role.permisos.map {|per| 
        per.tipo_permiso_id if per.can_see_index
      }.include?(items)
  	else
  		false
  	end
  end

  def self.search(filter, i_limit)
  	where(
      ['nombre like ? or email like ? or usuario like ?', 
            filter, filter, filter]
    ).limit(i_limit)
  end

  def to_s
  	usuario
  end
end

