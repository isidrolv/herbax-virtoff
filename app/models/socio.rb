class Socio < ActiveRecord::Base
  belongs_to :banco
  belongs_to :ciudad
  belongs_to :estado
  belongs_to :pais, :class_name => "Paise", :foreign_key => "pais_id"
  has_many :beneficiarios, :class_name => "Beneficiario", 
      :foreign_key => "socio_id", :dependent => :destroy
  has_many :socio_productos, :class_name => "SocioProducto", 
      :foreign_key => "socio_id", :dependent => :destroy
  has_one :socio_forma_pago, :class_name => "SocioFormaPago", 
      :foreign_key => "socio_id", :dependent => :destroy
  has_many :children, :class_name => "Socio", :foreign_key => "parent_id"
  belongs_to :parent, :class_name => "Socio", :foreign_key => "parent_id"
  has_one :usuario, :class_name => "Usuario", :foreign_key => "socio_id"

  accepts_nested_attributes_for :beneficiarios, 
    reject_if: proc { |attributes| attributes['nombre'].blank? }, 
    allow_destroy: true

  accepts_nested_attributes_for :socio_productos, 
    reject_if: proc { |attributes| attributes['cantidad'].blank? }, 
	  allow_destroy: true

  accepts_nested_attributes_for :socio_forma_pago, 
    reject_if: proc { |attributes| attributes['forma_pago'].blank? }, 
    allow_destroy: true

  validates_presence_of :numero, :message => "can't be blank"
  validates_uniqueness_of :numero, :message => "must be unique"
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,  :message => "is invalid"  
  
  before_update :do_before_update
  after_create :do_after_create

  def self.get_network(id)
    filter = "%" + id.to_s.rjust(5,'0') + "%"
    where('tree_path like ? and id <> ?', filter, id)
      .order(:tree_path)
  end

  def get_network
    Socio.get_network(self.id)
  end

  def self.network_depth(id)
    get_network(id).count    
  end

  def network_depth
    Socio.network_depth(self.id)
  end
  
  def do_before_update
    #p "executing Socio #{self.id} before_save"
    if self.id == self.parent_id
      raise "No pueden ser iguales id y el parent_id"
    else
      if has_children
        raise 'se esta usando un parent_id y ya es hijo'
      else
        update_tree
      end
    end
  end

  def do_after_create
    #p "executing Socio after_create"
    self.save
  end

  def update_tree
    node = self
    _tree_path = ""
    _node_level = 0
    parent_found = true
    while parent_found do 
      _node_level += 1
      _tree_path = node.id.to_s.rjust(5,'0') + "." + _tree_path
      #p _tree_path
      if node.parent_id
        #p "parent to search #{node.parent_id}"
        node = Socio.find(node.parent_id)
      else
        parent_found = false
      end
    end
    Socio.transaction do
      self.tree_path = _tree_path[0..-2]
      self.node_level = _node_level
      cuantos = Socio.where(parent_id: self.id).count
      #p "cuantos #{cuantos}"
      self.has_child = cuantos > 0
      #p "udpate Socio ended"
    end
    begin
      if !self.parent_id.nil?
        self.parent.save # update tree
      end
    rescue => e
      p e
    end
  end

  def socio_tree
    "-" * self.node_level + ">" + self.option
  end

  def ids
    self.id.to_s.rjust(3,'0')
  end

  def self.get_children(parent_id)
    begin
      #p "getting child nodes for #{parent_id}"
      options = Socio.where(parent_id: parent_id).order(:tree_path).map do |item|        
        { socio: item.nombre_completo, hasChild: item.has_child, 
          nodeLevel: item.node_level, children: Socio.get_children(item.id)
        }
      end
    rescue Exception => ex
      logger.info ex.to_s
    end
    return options
  end

  def self.as_array
    self.get_children nil
  end

  def has_children
    #p "children for #{self.id}"
    ids = children.map {|m| m.id} 
    #p ids
    #p ids.include? self.parent_id
    ids.include? self.parent_id
  end

  def nombre_completo
  	"#{nombre} #{apellido_paterno} #{apellido_materno}"
  end

  def to_s
    nombre_completo
  end

  def id_patrocinador
    if self.parent
      self.parent.numero
    end
  end

  def save_to_sih
    <<-SQL
      EXEC CL_sp_in_Cliente @psId_Cliente = '#{self.numero}', @pdF_Registro = '#{self.fecha_de_ingreso.strftime("%Y%m%d") if self.fecha_de_ingreso}', 
        @psA_Paterno = '#{self.apellido_paterno}', @psA_Materno = '#{self.apellido_materno}', @psNombre = '#{self.nombre}', 
        @psTipo_Cliente = 'I', @pnId_Pais_Origen = '1', @psId_Cliente_Nacional = '#{self.numero}', 
        @pnId_KitInscripcion = '1', @psId_Patrocinador = '#{self.id_patrocinador}', @psSexo = '1', 
        @pdF_Nacimiento = '#{self.fecha_nacimiento.strftime("%Y%m%d") if self.fecha_nacimiento}', @psDireccion = '#{self.domicilio_particular}', 
        @pnId_Colonia = '', @pnId_Ciudad = '', @pnId_Estado = '', @pnId_Pais = '', 
        @psA_Paterno_Conyuge = '', @psA_Materno_Conyuge = '', @psNombre_Conyuge = '', 
        @psTelefono = '#{self.telefono_particular}', @psFax = '', @psCelular = '#{self.telefono_celular}', @psEmail = '#{self.email}', 
        @psId_Tipo_Idenficacion = '', @psIdentificacion = '', @psId_Idioma = 'ES', 
        @psMetodo_Contacto = 'E-MAIL'
    SQL
  end

end

