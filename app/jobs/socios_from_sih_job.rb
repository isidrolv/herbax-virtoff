class SociosFromSihJob < ActiveJob::Base
  queue_as :default

  def perform
     Rails.logger.info 'Starting replication job for Socios from SIH to OV!'
     Rake::Task["db:migrar_socios"].invoke('100', '10')
  end
end
