module ApplicationHelper

  def company
    'herbax'
  end

  def app_title
    company.humanize + " Oficina virtual"  
  end

  def app_description
    "Nueva Oficina Virtual"
  end

  def copyright_notice
    "Oficina Virtual #{company.humanize} - Copyright (c) 2018 by #{company.humanize}"
  end

  def radio_group_toogle_bs(paises)
    str = '<div id="countries-btn" class="btn-group" data-toggle="buttons">'
    i = 1
    paises.each do | pais |
      str += "<label class='btn #{i == 1 ? 'btn-primary active focus' : 'btn-success' }'>"
      str += radio_button_tag "pais", pais[:id], checked=(i == 1), {autocomplete: "off"} 
      str += pais[:codigo]
      str += '</label>'
      i += 1
    end
    str += '</div>'
    str.html_safe
  end

  def sub_menu(prompt, links)
    str = '<li>'
    str += '<a class=\'dropdown-toggle\' data-toggle=\'dropdown\' aria-expanded=\'false\' href=\'#\'>'
    str += "#{prompt}<span class=\"caret\"></span></a>"
    str += '<ul class=\'dropdown-menu\' role=\'menu\'>'
    links.each do |prompt, link|
      str += "<li role='presentation'><a href='#{link}'>#{prompt}</a></li>"
    end
    str += '</ul>'
    str += '</li>'
    str.html_safe
  end

  def dropdown_sub_menu(item)
    if item[:option] == "--"
      str = "<li class='divider'></li>"
    else 
      if item[:hasChild] == true
        if item[:nodeLevel] == 1
          str = '<li>'
          str += '<a class=\'dropdown-toggle\' data-toggle=\'dropdown\' aria-expanded=\'false\' href=\'#\'>'
          str += "#{item[:option]}<span class='caret'></span></a>"
          str += '<ul class=\'dropdown-menu\' role=\'menu\'>'        
        else      
          str = "<li class='dropdown-submenu'> 
                                <a href='#{item[:target]}' class='dropdown-toggle' data-toggle='dropdown'>#{item[:option]}</a>
                                <ul class='dropdown-menu' role='sub-menu'>"
        end
        item[:children].each do |sublink|
          if sublink[:hasChild]
            str += dropdown_sub_menu(sublink)
          else
            #p "<#{sublink[:option]}>"
            if sublink[:option] == "--"
              str += "<li class='divider'></li>"
            else 
              str += "<li role='presentation'><a href=\"#{sublink[:target]}\">#{sublink[:option]}</a></li>"
            end
          end
        end
        str += "</ul></li>"
      else
        if item[:option] == "--"
          str = "<li class='divider'></li>"
        else 
          str = "<li role='presentation'><a href='#{item[:target]}'>#{item[:option]}</a></li>'"
        end
      end
    end

    str.html_safe
  end

  def error_explanation_dialog(entity, close_prompt="Cerrar")
    str = """
    <div id=\"error_explanation\" class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"errorExplanationLabel\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
            <h4 class=\"modal-title\" id=\"errorExplanationLabel\">"
    str += "<span>" + pluralize(entity.errors.count, "error") + " evitan que sea guardada la informacion:</span>"
    str += "</h4></div><div class=\"modal-body\"><ul>"
    entity.errors.full_messages.each do |message|
      str += '<li>' + message + '</li>'
    end
    str += '</ul></div><div class=\'modal-footer\'>'
    str += "<button type='button' class='btn btn-default btn-primary' data-dismiss='modal'>#{close_prompt}</button>"
    str += '</div></div></div></div>'
    str += "<script type=\"text/javascript\">$('#error_explanation').modal('show');</script>"
    str.html_safe
  end

  def form_text_field(form, field_name, class_attr, opts={})
    opts['class'] = 'form-control'
    str = "<div class=\"#{class_attr}\">"
    str += '<div class=\'form-group\'>'
    str += form.label(field_name, nil, {id: field_name})
    str += form.text_field(field_name, opts)
    str += '</div>'
    str += '</div>'
    str.html_safe
  end

  def form_text_area(form, field_name, class_attr, opts={})
    opts['class'] = 'form-control'
    str = "<div class=\"#{class_attr}\">"
    str += '<div class=\'form-group\'>'
    str += form.label(field_name, nil, {id: field_name})
    str += form.text_area(field_name, opts)
    str += '</div>'
    str += '</div>'
    str.html_safe
  end

  def form_collection_select(form, field_name, collection, field_id, field_prompt, class_attr, opts={})
    opts['class'] = 'form-control'
    str = "<div class=\"#{class_attr}\">"
    str += '<div class=\'form-group\'>'
    str += form.label(field_name, nil, {id: field_name})
    str += form.collection_select field_name, collection, field_id, field_prompt, {prompt: t('select.prompt')}, opts
    str += '</div>'
    str += '</div>'
    str.html_safe
  end

  def questionary_item(entity, item_id, prompt, value)
    str = '<div class=\'row table-bordered\'>'
    str += label_tag("#{entity}", prompt, class: 'col-sm-10 col-md-10 col-lg-10')
    str += "<div class='col-sm-1 col-md-1 col-lg-1'><div class='row'>"
    str += label_tag('SI', 'SI', {class: 'col-sm-6 col-md-6 col-lg-6'})
    str += radio_button_tag("#{entity}", true, value, {class: 'col-sm-6 col-md-6 col-lg-6'})
    str += '</div></div>'
    str += '<div class=\'col-sm-1 col-md-1 col-lg-1\'><div class=\'row\'>'
    str += label_tag('NO', 'NO', {class: 'col-sm-6 col-md-6 col-lg-6'})
    str += radio_button_tag("#{entity}", false, !value, {class: 'col-sm-6 col-md-6 col-lg-6'})
    str += '</div></div>'
    str += '</div>'
    str.html_safe
  end

  def disabler_script 
    str = "<script type='text/javascript'>"
    str += "$('.form-control').attr('disabled','true');"
    str += "$('input[type=radio]').attr('disabled', 'true');"
    str += "$('input[type=checkbox]').attr('disabled', 'true');"
    str += "$('#btnSearchZipCode').attr('disabled', 'true');"
    str += "$('input[name=commit]').hide();"
    str += "$('.add_fields').hide();"
    str += "</script>"
    str.html_safe
  end

  def glyph_icon(prompt, figure)
    "<i class=\"glyphicon glyphicon-#{figure}\"></i>&nbsp;#{prompt}".html_safe    
  end

  def search_box(field_name, message)
    str = "<div class='input-group'>"
    str += text_field_tag field_name, '', options={ type: 'text', class: 'form-control', placeholder: message}
    str += "<span class='input-group-btn'>"
    str += "<button class='btn btn-success' type='button'>"
    str += glyph_icon '', 'search' 
    str += "</button></span></div>"
    str.html_safe
  end
 
  def panel_heading(model)
    str = "<div class='panel-heading'>"
    str += "<div class='row'>"
    str += "<SPAN class='col-lg-6'><FONT name='arial' size='5' weight='bold' class='search-title' id='search-title-#{model.downcase}'>#{model}s</FONT></SPAN>"
    str += "<div class='col-lg-6'>"
    str += "</div></div></div>"
    str.html_safe
  end
       
  def panel_heading_with_search_box(model)
    str = "<div class='panel-heading'>"
    str += "<div class='row'>"
    str += "<SPAN class='col-lg-6'><FONT name='arial' size='5' weight='bold' class='search-title' id='search-title-#{model.downcase}'>#{model}s</FONT></SPAN>"
    str += "<div class='col-lg-6'>"
    str += search_box "txtSearch#{model}", "buscar #{model.downcase} por ..."
    str += "</div></div></div>"
    str.html_safe
  end
  
  def current_controller?(options)
    url_string = CGI.escapeHTML(url_for(options))
    params = ActionController::Routing::Routes.recognize_path(url_string, :method => :get)
    params[:controller] == @controller.controller_name
  end

  def version
    '1.0'
  end

  def release
    "prd"
  end

  def ph
    session[:permisos_hash]
  end

  def link_to_add_fields(name, f, association, css_class)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize + "_fields", f: builder)
    end
    link_to(name, '#', class: "#{css_class} add_fields" , 
      data: {id: id, fields: fields.gsub("\n",'')})
  end

  def link_to_show(object)
    link_to object, options={class: "btn btn-primary btn-xs"} do
      glyph_icon 'Ver', 'eye-open'
    end
  end

  def link_to_edit(path)
    link_to path, options={class: "btn btn-info btn-xs"} do
      glyph_icon 'Editar', 'pencil'
    end
  end

  def link_to_edit_this(path)
    link_to path, options={class: "btn btn-primary"} do
      glyph_icon 'Editar', 'pencil'
    end
  end

  def link_to_destroy(object)
    link_to object, method: :delete, data: { confirm: '¿Está usted seguro(a)?' }, class: "btn btn-danger btn-xs" do
      glyph_icon 'Eliminar', 'minus'
    end    
  end

  def link_to_back(path)
    link_to 'Volver', path, class: "btn btn-info" 
  end

  def forma_pago_radio_group(builder, forma_pago)
    str = ""
    ["VISA", "MasterCard", "OXXO", "Cuenta Bancaria"].each do | key | 
      str += "<div style='margin-left: 10px; float: left'>"
      str += builder.radio_button forma_pago, key 
      str += "&nbsp;"
      str += builder.label forma_pago, key 
      str += "</div>"
    end
    str.html_safe
  end
end
