# cronotab.rb — Crono configuration file
#
require 'rake'

Rails.app_class.load_tasks

Crono.perform(SociosFromSihJob).every 6.hour, at: {hour: 2, min: 00}

