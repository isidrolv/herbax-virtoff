Rails.application.routes.draw do
  resources :cliente_elites
  resources :socios
  resources :banco
  resources :dashboard
  resources :compras_personales
  resources :estado_cuenta
  resources :plan_de_negocios
  resources :facturas
  resources :proveedors
  resources :precios
  resources :conceptos
  resources :pagos
  resources :asentamientos
  resources :paises
  resources :estados  
  resources :ciudads
  resources :categories
  root to: 'dashboard#index'
  post '/categories/search'
  post '/socios/search'
  post '/cliente_elites/search'
  post '/paises/search'

  devise_for :usuarios
  scope '/admin' do
    resources :usuarios
    post '/usuarios/search'
    resources :permisos
    resources :roles
  end
end
