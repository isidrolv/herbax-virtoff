#  cr_socio_usr.rake
#  permite generar usuarios y socios a partir de la BD de SIH
#  ejemplos:
#    $ rake create:socios_usuarios

require 'active_record'
require 'yaml'
require 'tiny_tds'

namespace :db do
 
  def open_db
    Rails.logger.info "open_db"
    config2 = YAML.load_file("#{Rails.root.to_s}/config/database.yml")
    username = config2["development"]["username"]
    password = config2["development"]["password"]
    host = config2["development"]["host"]
    dataserver = config2["development"]["dataserver"]
    database = config2["development"]["sih_database"]
    dataserver = config2["development"]["dataserver"]
    
    Rails.logger.info config2
    TinyTds::Client.new username: username, password: password, host: host, 
        dataserver: dataserver, database: database, timeout: 10000
  end

  def cuantos
    client = open_db
    Rails.logger.info client
    rs = client.execute <<-SQL 
    SELECT count(*)
    FROM [db_SIH].[dbo].[CL_tb_Cliente]
    where Id_Cliente collate SQL_Latin1_General_CP1_CI_AS not in ( 
      select numero from [herbax_virtoff_development].[dbo].[socios] 
    )
    SQL
    Rails.logger.info rs
    rs.map{|row| row }.first[""]
  end

  def get_new_socios(filter)
    Rails.logger.info "get_new_socios"
    client = open_db
    Rails.logger.info client
    rs = client.execute <<-SQL 
    SELECT #{filter} *
    FROM [db_SIH].[dbo].[CL_tb_Cliente]
    where Id_Cliente collate SQL_Latin1_General_CP1_CI_AS not in ( 
      select numero from [herbax_virtoff_development].[dbo].[socios] 
    )
    SQL
    data = rs.map{|row| row}
    Rails.logger.info data
    Rails.logger.info data.count
    rs
  end
  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  
  def crea_nuevos(rs)
    rs.each do | row |
      begin
        socio = row.map {|key, value| [key, value.is_a?(BigDecimal) ? value.to_f : value]}.to_h
        e_mail = if VALID_EMAIL_REGEX.match(socio["Email"])
          socio["Email"]
        else
          "s_#{socio["Id_Cliente"]}@herbax.com.mx"
        end

        Rails.logger.info "Sincronizando socio #{socio["Id_Cliente"]} y creando su usuario con email = #{socio["Email"]} / #{e_mail}"

        u = Usuario.create usuario: "S"+socio["Id_Cliente"], password: socio["Id_Cliente"], 
          password_confirmation: socio["Id_Cliente"], 
          nombre: socio["Nombre"] + " " + socio["A_Paterno"] + " " + socio["A_Materno"], 
          email: e_mail, activo: true, id_cliente: socio["Id_Cliente"], role_id: 2
        s = Socio.create numero: socio["Id_Cliente"], nombre: socio["Nombre"], 
          apellido_paterno: socio["A_Paterno"], apellido_materno: socio["A_Materno"], 
          email: e_mail
         parent = Socio.where(numero: socio["Id_Patrocinador"])
        if parent.count > 0
          s.parent_id = parent.first.id
          s.save
        end
        u.socio_id = s.id
        u.save
      rescue Exception => e
        Rails.logger.info e
      end
    end
  end
  
  desc 'Crea Usuarios y Socios tomando la base de datos de SIH'
  task :migrar_socios, [:limite, :repeticiones] => [:environment] do |task, args|
    
    Rails.logger.info "replicating socios..."

    vueltas = 1
    result_set = get_new_socios("top #{args[:limite]}")
    while result_set.count > 0
      crea_nuevos(result_set)
      result_set = get_new_socios("top #{args[:limite]}")
      vueltas += 1
      break if vueltas > args[:repeticiones].to_i
    end

  end # task

end
