#  export.rake
#  permite exportar datos de la base de datos a archivos .seed
#  ejemplos:
#    $ rake export:seeds_format
#    $ rake export:seeds_format > db/seeds/export.seeds.rb
#    $ rake export:scaffold > scaffold.bat

namespace :export do
  def load_models
    # load all models into memory to be mapped
    Dir[Rails.root.to_s + '/app/models/**/*.rb'].each do |file|
      begin
        require file
      rescue => e
        p e.to_s
      end
    end
  end

  desc 'Prints all models in a seeds.rb way.'
  task seeds_format: :environment do
    load_models
    # get model names
    models = ActiveRecord::Base.subclasses.collect { |type| type[:name] }.sort
    models.each do |model|
      # initialize model class and gets all records
      begin
        # validate if parent_id exists in table
        instance = model.constantize.order(:parent_id)
        ids = instance.map { |m| m[:id] }
        p "# read records for #{model} #{ids}"
        instance = nil
        excluded_fields = %w(
          created_at updated_at id parent_id treepath tree_path
        )
        instance = model.constantize.all
        instance.each do |record|
          create_fields = record.serializable_hash.delete_if do |key, _|
            excluded_fields.include?(key)
          end
          create_fields.to_s.gsub(/[{}]/, '').gsub(/'([A-z_0-9]*)'\=>/, '\1: ')
          puts "#{model}.create(#{create_fields})"
        end
        instance2 = model.constantize.all
        instance2.each do |record|
          create_fields = record.serializable_hash.delete_if do |key, _|
            %w(parent_id).not_include?(key)
          end
          create_fields.to_s.gsub(/[{}]/, '').gsub(/'([A-z_0-9]*)'\=>/, '\1: ')
          p "#{model}.find(#{record.id}).update_attributes (#{create_fields})"
        end
      rescue => e
        p "## exception: #{e}"
        instance = nil
        instance = model.constantize.all
        instance.each do |record|
          create_fields = record.serializable_hash.delete_if do |key, _|
            %w(created_at updated_at id).include?(key)
          end
          create_fields.to_s.gsub(/[{}]/, '').gsub(/'([A-z_0-9]*)'\=>/, '\1: ')
          puts "#{model}.create(#{create_fields})"
        end
      end
    end
  end

  desc 'Exports all created models into a scaffold generation script'
  task scaffold: :environment do
    load_models
    # get model names
    models = ActiveRecord::Base.subclasses.collect { |type| type[:name] }.sort
    models.each do |model|
      # initialize model class and gets all records
      instance = model.constantize
      fields = ''
      columns = instance.columns_hash.delete_if do |key, _|
        %w(created_at updated_at id).include?(key)
      end
      columns.each do |col_name, value|
        fields += "#{col_name}:#{value.type} "
      end
      puts "rails generate scaffold #{model} #{fields}"
    end
  end

  desc 'Export all created models into a ddl sql script'
  task ddl: :environment do
    load_models
    sql = {
      'string' => 'varchar(255)', 'integer' => 'integer', 'float' => 'float',
      'date' => 'date', 'datetime' => 'timestamp', 'text' => 'text'
    }
    models = ActiveRecord::Base.subclasses.collect { |type| type[:name] }.sort
    models.each do |model| # initialize model class and gets all records
      instance = model.constantize
      table_def = "create table #{model.downcase} ("
      columns = instance.columns_hash.delete_if do |key, _|
        %w(created_at updated_at).include?(key)
      end
      columns.each do |col_name, val|
        sql_type = sql[val.type.downcase[0..-1]]
        a_col_name = col_name == 'id' ? 'not null primary key' : ''
        table_def += "#{col_name} #{sql_type} #{a_col_name}, "
      end
      table_def = table_def[0..-4]
      table_def += ');'
      puts table_def
    end
  end

  desc 'Export all created models into java beans'
  task bean: :environment do
  end

  desc 'Export all created models into DAO java classes'
  task bean: :environment do
  end
end
