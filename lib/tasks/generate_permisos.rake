# generate_permisos.rake
# permite exportar permisos a .seed
# ejemplos:
#   $ rake generate_permisos > db/seeds/permisos_export.seeds.rb

namespace :generate_permisos do
  desc 'Genera codigo base para la matriz de permisos'
  task base: :environment do
    modulo = [
      'Balance', 'Cuentas por Pagar', 'Registro y Programacion de Pagos',
      'Cuentas por Cobrar', 'Almacen', 'Empresas', 'Paises', 'Estados',
      'Ciudades', 'Materiales', 'Precios', 'Almacenes', 'Cuentas Bancarias',
      'Contactos', 'Bancos', 'Usuarios', 'Roles', 'Menus', 'Permisos',
      'Parametros'
    ]
    tipo_accion = {
      0 => 'accesar', 1 => 'listar', 2 => 'ver', 3 => 'agregar',
      4 => 'modificar', 5 => 'eliminar', 6 => 'imprimir', 7 => 'autorizar'
    }
    modulo.each do |item|
      tipo_accion.each do |key, value|
        nuevos = key == 3 ? ' nuevos ' : ''
        atoken = [1, 2, 4].include?(key) ? ' datos de ' : nuevos
        permiso = "#{value.humanize}#{atoken} #{item.humanize}"
        lines [
          "Permiso.create(descripcion: '#{permiso}'",
          "permiso: '#{permiso}'",
          "tipo_accion: #{key}, parent_id: nil)"
        ]
        code = lines.join(', ')
        puts code.gsub(/[ ][ ]/, ' ')
      end
    end
  end
end
