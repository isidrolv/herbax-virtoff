namespace :db do
  task extract_fixtures: :environment do
    sql = 'SELECT * FROM '
    skip_tables = %w(schema_migrations)
    ActiveRecord::Base.establish_connection
    tables = ENV['TABLES']
    if tables.nil?
      tables = ActiveRecord::Base.connection.tables - skip_tables
    else
      tables = tables.split(/,[ ]*/)
    end
    the_output_dir = ENV['OUTPUT_DIR']
    if the_output_dir.nil?
      output_dir = "#{Rails.root}/spec/fixtures"
    else
      output_dir = the_output_dir.sub(/\/$/, '')
    end
    tables.each do |table_name|
      i = '000'
      File.open("#{output_dir}/#{table_name}.yml", 'w') do |file|
        stm = sql + table_name.upcase
        data = ActiveRecord::Base.connection.select_all(stm)
        dump = data.inject({}) do |hash, record|
          p table_name + record
          hash["#{table_name}_#{i.succ!}"] = record
          hash
        end
        file.write dump.to_yaml
        puts "wrote #{table_name} to #{output_dir}/"
      end
    end
  end
end
