REPORTS_PATH = [Dir.pwd, '/app', '/pdfs', '/estado-cuenta-pdf.rb'].join
p REPORTS_PATH
EDO_CUENTA_DAO = [Dir.pwd,'/lib/estado_cuenta_dao.rb'].join

require 'tiny_tds'
require "#{REPORTS_PATH}"
require "#{EDO_CUENTA_DAO}"

# def get_data(params)
#   periodo = params[:periodo]
#   cliente = params[:cliente]
#   almacen = params[:almacen]

#   stored_procs = [
#     "EXEC CO_sp_se_Enc_EstadoDeCuenta #{periodo},'#{cliente}', #{almacen}",
#     "EXEC CO_sp_se_Dtl_EstadoDeCuenta #{periodo},'#{cliente}', #{almacen}",
#     "EXEC CO_sp_se_CalculoPercDed_PorCliente #{periodo},'#{cliente}', #{almacen}",
#     "EXEC CO_sp_se_VolumenCompraPorNivel #{periodo},'#{cliente}', #{almacen}",
#     "EXEC CO_sp_se_SociosPorGeneracion #{periodo},'#{cliente}', #{almacen}",
#     "EXEC CO_sp_se_Dtl_EstadoDeCuentaNuevos #{periodo},'#{cliente}', #{almacen}"
#   ]

#   client = open_db

#   results = []

#   stored_procs.each_with_index do | sproc, index |
#     p sproc
#     result_set = client.execute(sproc)
#     if result_set.count == 0
#       raise "No hay datos para el socio #{cliente}"
#     end
#     results[index] = result_set.map {|row| row.map {|key, value| [key, value.is_a?(BigDecimal) ? value.to_f : value]}.to_h }
#     #p "r[i] =>", results[index]
#   end
#   results
# end

# def open_db
#   TinyTds::Client.new username: 'sa', password: 'S4ndra21', host: 'localhost', 
#     dataserver: 'ACER-ISIDRO\SQLEXPRESS2012', database: 'db_SIH_Edo', timeout: 10000
# end

# def get_periodo(id_periodo)
#   client = open_db
#   periodos = client.execute "SELECT * FROM CU_tb_Periodos where id_periodo = #{id_periodo}"
#   per = periodos.first
#   p per
#   per["Inicio"].strftime("%^B %Y")
# end

data = get_data({cliente: '0000173831', periodo: 293, almacen: -1})
p data
periodo = get_periodo periodo = 293

pdf = EstadoCuentaPDF.new data, periodo
pdf.render_file 'temp.pdf'

