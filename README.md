# Herbax S.A. Portal de Oficina Virtual #

El presente proyecto contiene el c�digo fuente de la nueva versi�n de Oficina Virtual de Herbax.com

El desarrollo est� realizado en Ruby on Rails versi�n 4.2.5 y se montar� sobre un servidor Windows. Utiliza una base de datos
MS SQL Server 2012 y Bootstrap para apariencia responsiva.

## Pre-requisitos ##

1. Ruby 2.2.4p230
1. Rails 4.2.5
1. Bootstrap 3.3
1. Git
1. MS SQL Server 2012

## Instrucciones para descargar en local ##

1. Abrir una ventana de comandos de windows (CMD, Git CMD o Git Bash)
1. Moverse a una carpeta donde se desea tener el repositorio. p.e. cd /proyectos
1. Clonar el repositorio usando el comando:
**git clone git@bitbucket.org:isidrolv/herbax-virtoff.git**
se le pedir� usuario y contrase�a (usar sus credenciales de bitbucket)
1. Una vez terminada la descarga ejecute las ordenes: 
* cd herbax-virtoff
* bundle install -- para instalar todas las gemas

## Preparar la base de datos ##
para preparar la base de datos ejecute los siguiente comandos desde la consola de windows, dentro de la carpeta del proyecto (herbax-virtoff):

1. bundle exec rake db:create
1. bundle exec rake db:migrate
1. bundle exec rake db:seed

## Para actualizar version local de master para hacer pruebas de usuario ##

1. hacer commit a todos los cambios del branch en el que se est� trabajando: git add . && git commit -m "comentarios ..."
1. git checkout master
1. bundle exec db:drop:all
1. bundle exec db:create:all
1. bundle exec db:migrate
1. bundle exec db:seed


## Para iniciar la aplicacion ##

Para iniciar la aplicacion ejecute el comando:

1. bundle exec rails s 